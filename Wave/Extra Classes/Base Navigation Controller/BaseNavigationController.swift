//
//  BaseNavigationController.swift
//  Wave
//
//  Created by Vengile on 07/06/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController, UINavigationControllerDelegate {
	
	
	/****************************************************************************************************************/
	// MARK: - Override Methods
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		navigationBar.barTintColor = Constants.kNavigationcolor
		
//		navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//		navigationBar.shadowImage = UIImage()
		navigationBar.isTranslucent = true
		
        
        navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.init(name: "MyriadPro-Regular", size: 18.0)!,
                                             NSForegroundColorAttributeName: UIColor.white]
		delegate = self;
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	
	/****************************************************************************************************************/
	// MARK: - Class Methods
	
	func AddTitle(_ target: UIViewController, title : String) {
		let titleLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 30.0))
		titleLabel.text = title;
		titleLabel.textColor = UIColor.white
		titleLabel.backgroundColor = UIColor.clear
		titleLabel.textAlignment = .center
		titleLabel.font = UIFont.init(name: "HelveticaNeue-Medium", size: 15.0)
		target.navigationItem.titleView = titleLabel
	}
	
	func AddTitleOnSignUp(_ target: UIViewController, title : String) {
		let titleLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 30.0))
		titleLabel.text = title;
		titleLabel.textColor = UIColor.gray
		titleLabel.backgroundColor = UIColor.clear
		titleLabel.textAlignment = .center
		titleLabel.font = UIFont.init(name: "HelveticaNeue-Medium", size: 15.0)
		target.navigationItem.titleView = titleLabel
	}
	
	
	func AddImageInTitle(_ target: UIViewController){
		let logo = UIImage(named: "logo")
		let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 35))
		imageView.image = logo
		imageView.contentMode = .scaleAspectFit
		target.navigationItem.titleView = imageView
		
	}
	
	
	func AddBackGroundImage() {
		
		let imageBG = UIImage.init(named: "NavigationBG")
		imageBG?.draw(in: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
		self.navigationBar.setBackgroundImage(imageBG, for: .default)
		
		
	}
	
	func addMenuButtonOn(_ target: UIViewController, selector: Selector) {
		let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50.0))
		button.setImage(UIImage.init(named: "Menu"), for: .normal)
		button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
		
		let barButtonItem = UIBarButtonItem.init(customView: button)
		target.navigationItem.leftBarButtonItem = barButtonItem
	}
	
	func addRightButton(_ target: UIViewController, selector: Selector , image : UIImage) {
		let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 25.0, height: 30.0))
		button.setImage(image, for: .normal)
		
		button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
		
		let barButtonItem = UIBarButtonItem.init(customView: button)
		target.navigationItem.rightBarButtonItem = barButtonItem
	}
	
	func addRLeftButton(_ target: UIViewController, selector: Selector , image : UIImage) {
		let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 25.0, height: 30.0))
		button.setImage(image, for: .normal)
		
		button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
		
		let barButtonItem = UIBarButtonItem.init(customView: button)
		target.navigationItem.leftBarButtonItem = barButtonItem
	}
	
	
	func addRightButtonWithTitle(_ target: UIViewController, selector: Selector , lblText : String) {
		
		let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 50.0, height: 30.0))
		button.setTitle(lblText, for: .normal)
		button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
		button.titleLabel!.font =  UIFont.systemFont(ofSize: 12.0)
		button.backgroundColor = UIColor.clear
		
		
		let barButtonItem = UIBarButtonItem.init(customView: button)
		target.navigationItem.rightBarButtonItem = barButtonItem
	}
	
	
	
	func addBackButtonOn(_ target: UIViewController, selector: Selector) {
		let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 30.0, height: 30.0))
		button.setImage(UIImage.init(named: "back"), for: UIControlState())
		button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
		
		let barButtonItem = UIBarButtonItem.init(customView: button)
		target.navigationItem.leftBarButtonItem = barButtonItem
	}
    
    
    
    func addLeftMenuButtonOn(_ target: UIViewController, selector: Selector) {
        let button = UIButton.init(frame: CGRect(x: 0, y: 0, width: 36, height: 30.0))
        button.setImage(UIImage.init(named: "Menu-Icon"), for: .normal)
        button.addTarget(target, action: selector, for: UIControlEvents.touchUpInside)
        
        let barButtonItem = UIBarButtonItem.init(customView: button)
        target.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    
    func addProfileCountViewOn(_ target: UIViewController, selector: Selector) {
        let profileCountView = Bundle.main.loadNibNamed("ProfileCountView", owner: self, options: nil)?[0] as! ProfileCountView
        if let path = WaveUser.shared.imagepath {
            profileCountView.profilePic.sd_setImage(with: URL.init(string: path), completed: nil)
        }
        profileCountView.button.addTarget(target, action: selector, for: .touchUpInside)
        
        let barButtonItem = UIBarButtonItem.init(customView: profileCountView)
        target.navigationItem.rightBarButtonItem = barButtonItem
    }
}
