//
//  ProfileCountView.swift
//  Wave
//
//  Created by Vengile on 11/07/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class ProfileCountView: UIView {

    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var countLabel: UILabel!
    @IBOutlet var button: UIButton!
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
//        profilePic.contentMode = .scaleAspectFit
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2.0
    }
}
