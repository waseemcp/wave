//
//  WaveUser.swift
//  Wave
//
//  Created by Jawad Ahmed on 12/16/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import CoreLocation

class WaveUser: NSObject {
    var sessionId: String?
    var id: Int?
    var fbId: Int?
    var name: String?
    var email: String?
    var gender: String?
    var age: Int?
    var imagepath: String?
    var isAdmin: Bool?
    var martialStatus: Int?
    var location: CLLocation?
    
    static var shared = WaveUser()
    
    
    
    override init() {
        super.init()
    }
    
    
    func create(with details: [String: Any]) {
        sessionId = details["session_id"] as? String
        id = details["id"] as? Int
        fbId = details["fb_id"] as? Int
        name = details["name"] as? String
        email = details["email"] as? String
        gender = details["gender"] as? String
        age = details["age"] as? Int
        imagepath = details["user_image"] as? String
        isAdmin = details["is_admin"] as? Bool
        martialStatus = details["marital_status"] as? Int
        let user = User()
        user.sessionID = sessionId ?? ""
        user.ID = details["id"] as? String ?? ""
        user.FBID = details["fb_id"] as? String ?? ""
        user.email = email ?? ""
        user.gender = gender ?? ""
        user.age = age
        user.imagepath = imagepath ?? ""
        user.isAdmin = isAdmin
        user.martialStatus = martialStatus
        user.userName = name ?? ""
        UserDefaults.standard.set(100, forKey: "radius")
        //user.profileImage = imagepath ?? ""
    
        DataManager.sharedInstance.user = user
        DataManager.sharedInstance.saveUserPermanentally()
    }
}
