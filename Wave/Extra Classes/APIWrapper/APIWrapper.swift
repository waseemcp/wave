//
//  APIWrapper.swift
//  Wave
//
//  Created by Jawad Ahmed on 12/16/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import Alamofire

class APIWrapper: NSObject {
    
    class func getRequest(with url: String, params: [String: Any]?, completion: @escaping (Any) -> Void) {
        let fullURL = "http://139.162.37.73/wave_app/public/api/" + url
        LoadingView.show()
        
        var headers: HTTPHeaders?
        
        if let session = WaveUser.shared.sessionId {
            headers = ["Authorization": session]
        }
        print(params)
        Alamofire.request(fullURL, method: .get, parameters: params!, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            switch response.result {
            case .success(let response):
                LoadingView.hide()
                print("API Response: \(response)")
                
                if let data = response as? [String: Any] {
                    if let status = data["status"] as? Bool {
                        if status == true {
                            completion(data["data"] ?? [:])
                        }
                        else {
                            let errorDict = data["error"] as! [String: Any]
                            let messages = errorDict["messages"] as? [String]
                            let messageString = messages?.joined(separator: "\n")
                            
                            
                            
                            if errorDict["code"] as! Int == 401 {
//                                Utilities.showAlert(with: "Error", message: "Logout ")
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Logout"), object: nil)
                            }else {
                                Utilities.showAlert(with: "Error", message: messageString)
                            }
                        }
                    }
                }
                
            case .failure(let error):
                LoadingView.hide()
                Utilities.showAlert(with: "Error", message: error.localizedDescription)
                print("API Error: \(error)")
            }
        }
    }
    
    
    class func postRequest(with url: String, params: [String: Any]?, image: UIImage?, completion: @escaping (Any) -> Void) {
        let fullURL = "http://139.162.37.73/wave_app/public/api/" + url
        LoadingView.show()
        
        var headers: HTTPHeaders?
        if let session = WaveUser.shared.sessionId {
            headers = ["Authorization": session]
        }
        
        Alamofire.upload(multipartFormData: { (formData) in
            if let paramDict = params {
                for (key, value) in paramDict {
                    formData.append(String(describing: value).data(using: .utf8)!, withName: key)
                }
            }
            
            if (image != nil) {
                let imageData = UIImagePNGRepresentation(image!)
                formData.append(imageData!, withName: "user_image", fileName: "file.png", mimeType: "image/png")
            }
        }, usingThreshold: UInt64(),
           to: fullURL,
           method: .post,
           headers: headers) { (encodingResult) in
            switch encodingResult {
            case .success(request: let upload, streamingFromDisk: _, streamFileURL: _):
                upload.responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success(let responseObject):
                        LoadingView.hide()
                        print("API Response: \(responseObject)")
                        
                        if let data = responseObject as? [String: Any] {
                            if let status = data["status"] as? Bool {
                                if status == true {
                                    completion(data["data"] ?? [:])
                                }
                                else {
                                    let errorDict = data["error"] as! [String: Any]
                                    let messages = errorDict["messages"] as? [String]
                                    let messageString = messages?.joined(separator: "\n")
//                                    Utilities.showAlert(with: "Error", message: messageString)
                                    if errorDict["code"] as! Int == 401 {
                                        //                                Utilities.showAlert(with: "Error", message: "Logout ")
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Logout"), object: nil)
                                    }else {
                                        Utilities.showAlert(with: "Error", message: messageString)
                                    }
                                }
                            }
                        }
                        break
                        
                    case .failure(let error):
                        LoadingView.hide()
                        Utilities.showAlert(with: "Error", message: error.localizedDescription)
                        print("API Error: \(error)")
                        break
                    }
                })
                
            case .failure(let error):
                LoadingView.hide()
                Utilities.showAlert(with: "Error", message: error.localizedDescription)
                print("API Error: \(error)")
                break
            }
        }
    }
}
