//
//  LoadingView.swift
//  Wave
//
//  Created by Jawad Ahmed on 12/16/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    class func show() {
        let window = UIApplication.shared.keyWindow!
        
        let loadingView = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)?.first as! LoadingView
        loadingView.tag = -999
        loadingView.frame = window.bounds
        
        window.addSubview(loadingView)
    }
    
    
    class func hide() {
        let window = UIApplication.shared.keyWindow!
        if let loadingView = window.viewWithTag(-999) as? LoadingView {
            loadingView.removeFromSuperview()
        }
    }
}
