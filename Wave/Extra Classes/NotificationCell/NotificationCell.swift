//
//  NotificationCell.swift
//  Wave
//
//  Created by Vengile on 7/11/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setText(_ text: String, clubName: String, distance: String) {
        let completeString = text + " " + distance
        let attributedString = NSMutableAttributedString.init(string: completeString)
        
        let clubNameRange = (completeString as NSString).range(of: clubName)
        let distanceRange = (completeString as NSString).range(of: distance)
        
        let commonAttributes = [NSForegroundColorAttributeName: UIColor.white,
                                NSFontAttributeName:  UIFont.init(name: "MyriadPro-Regular", size: 15.0)!]
        
        attributedString.addAttributes(commonAttributes, range: NSRange.init(location: 0, length: completeString.characters.count))
        attributedString.addAttributes([NSForegroundColorAttributeName: Constants.kYellowColor], range: clubNameRange)
        attributedString.addAttributes([NSForegroundColorAttributeName: Constants.kSilverColor], range: distanceRange)
        
        titleLabel.attributedText = attributedString
    }
}
