//
//  ListViewCell.swift
//  Wave
//
//  Created by Vengile on 7/11/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import SDWebImage

class ListViewCell: UITableViewCell {

    @IBOutlet var mainImageView: UIImageView!
    @IBOutlet var feeLabel: UILabel!
    @IBOutlet var starImageViews: [UIImageView]!
    @IBOutlet var placeNameLabel: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var peopleCountLabel: UILabel!
    @IBOutlet var maleBarView: UIView!
    @IBOutlet var femaleBarView: UIView!
    
    @IBOutlet var maleBarWidth: NSLayoutConstraint!
    @IBOutlet var femaleBarWidth: NSLayoutConstraint!
    
    let totalBarWidth: Float = 60.0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        feeLabel.layer.cornerRadius = 8.0
        feeLabel.clipsToBounds = true
    }
    
    
    func updateWithInfo(_ info: [String: Any]) {
        // Image
        if let imagesArray = info["images"] as? [[String: Any]], imagesArray.count > 0 {
            let imageDict = imagesArray.first
            mainImageView.sd_setShowActivityIndicatorView(true)
            mainImageView.sd_setImage(with: URL.init(string: imageDict?["image_path"] as! String), completed: nil)
        }
        else {
            mainImageView.image = nil
        }
        
        // Fee
        if let price = info["price"] as? String {
            if price == "0" {
                feeLabel.text = "   Free   "
            }
            else {
                feeLabel.text = "   $\(price) Cover Charge   "
            }
        }
        
        // Rating
        if let rating = info["no_of_ratings"] as? Int {
            for i in 0...4 {
                if i < rating {
                    starImageViews[i].image = UIImage.init(named: "b_star")
                    continue
                }
                else {
                    starImageViews[i].image = UIImage.init(named: "g_star")
                }
            }
        }
        
        // Place Name
        placeNameLabel.text = info["name"] as? String
        
        // Distance
        if let distance = info["distance"] as? Float {
            distanceLabel.text = String.init(format: "%.1f Miles Away", distance)
        }
        else {
            distanceLabel.text = "Not available"
        }
        
        // Peaople Count
        if let count = info["check_in_count"] as? Int {
            peopleCountLabel.text = "\(count)"
        }
        
        //Male vs Female Bar
        if let maleCount = info["male_count"] as? Int {
            if let femaleCount = info["male_count"] as? Int {
                if maleCount == femaleCount {
                    maleBarWidth.constant = CGFloat(totalBarWidth/2.0)
                    femaleBarWidth.constant = CGFloat(totalBarWidth/2.0)
                }
            }
        }
    }
}
