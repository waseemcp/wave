//
//  Utilities.swift
//  Wave
//
//  Created by Jawad Ahmed on 12/16/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class Utilities: NSObject
{
    class func showAlert(with title: String?, message: String?) {
        let alertController: UIAlertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction.init(title: "OK", style: .destructive, handler: nil))
        alertController.show()
    }
}
