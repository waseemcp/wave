//
//  File.swift
//  Wave
//
//  Created by Vengile on 08/06/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import Foundation
import UIKit

class BaseWhiteCornerButton: UIButton {
	
	override init(frame: CGRect) {
		super.init(frame: frame)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		//custom logic goes here
		self.CornerRadious()
	}
	
	func CornerRadious()  {
		//self.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
		self.layer.cornerRadius = Constants.kCornerRaious
		self.layer.borderColor = UIColor.lightGray.cgColor
		self.layer.borderWidth = 1
		
		self.layer.masksToBounds = true
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
