//
//  APIConstant.swift
//  SaveMe
//
//  Created by Vengile on 26/04/2017.
//  Copyright © 2017 Vengile. All rights reserved.
//

import Foundation

//MARK:- API Keys
let kPasswordKey        = "password"
let kEmailKey           = "email"

let kSocialNameKey      = "name"
let kSocialIDKey        = "id"
let kPatientIDKey       = "patient_id"
let kFBIDKey            = "fb_id"
let kSocialFNameKey     = "first_name"
let kSocialLNameKey     = "last_name"

let kUserNameKey        = "user_name"
let kFullNameKey        = "full_name"

let kDeviceIDKey        = "device_id"
let kDeviceTypeKey      = "device_type"
let kRegisterTypeKey    = "user_type"
let kUserRoleKey        = "user_role"
let kEContactKey        = "emergency_contact"
let kProfileAddKey      = "profile_section"
let kChatIDKey		    = "chat_id"

let kAgeKey             = "age"
let kBloodTypeKey       = "blood_type"
let kDLNumberKey        = "dl_number"
let kDNRKey             = "dnr"
let kGenderKey          = "gender"
let kIDKey              = "ID"
let kLocationKey        = "location"
let kSessionIDKey       = "session_id"
let kStateKey           = "state"
let kUserImageKey       = "user_image"
let kUserSSN				= "ssn"
let kDocumentCount      = "medical_document_count"
let kHistoryCount       = "medical_history_count"

let kUserSessionKey     = "Authorization"

let kProfilePictureURLKey = "ProfilePictureURL"


let kSessionKey        = "session_id"

let kUser_ID            = "id"
let kUser_name        = "name"
let kUser_lastName        = "lastName"
let kUser_fb_id        = "fb_id"
let kUser_gender        = "gender"
let kUser_age        = "age"
let kUser_user_image        = "user_image"
let kUser_device_id        = "device_id"
let kUser_device_type        = "device_type"



let BloodTypeArray      = ["A+" , "A-" , "B+" , "B-" , "AB+" , "AB-" , "O+" , "O-"]
let GenderArray         = ["Male" , "Female" , "Other"]

let USAStates = ["Alaska",
              "Alabama",
              "Arkansas",
              "American Samoa",
              "Arizona",
              "California",
              "Colorado",
              "Connecticut",
              "District of Columbia",
              "Delaware",
              "Florida",
              "Georgia",
              "Guam",
              "Hawaii",
              "Iowa",
              "Idaho",
              "Illinois",
              "Indiana",
              "Kansas",
              "Kentucky",
              "Louisiana",
              "Massachusetts",
              "Maryland",
              "Maine",
              "Michigan",
              "Minnesota",
              "Missouri",
              "Mississippi",
              "Montana",
              "North Carolina",
              " North Dakota",
              "Nebraska",
              "New Hampshire",
              "New Jersey",
              "New Mexico",
              "Nevada",
              "New York",
              "Ohio",
              "Oklahoma",
              "Oregon",
              "Pennsylvania",
              "Puerto Rico",
              "Rhode Island",
              "South Carolina",
              "South Dakota",
              "Tennessee",
              "Texas",
              "Utah",
              "Virginia",
              "Virgin Islands",
              "Vermont",
              "Washington",
              "Wisconsin",
              "West Virginia",
              "Wyoming"]

enum RegisterType: String {
    case Email = "0"
    case Facebook = "1"
}

enum PickerShowOptions: String {
    case Gender = "0"
    case BloodType = "1"
	case State = "2"
}

enum UserType: String {
    case Patient        = "0"
    case FirstReponder  = "1"
	case Doctor  = "2"
}

enum WebServiceName: String {
    
    
    /****   Basic APIS  *******/
    case ChangePassword		= "change/password"
    case ForgotPassword		= "forgot/password"
    case Login				= "user/login"
	case LoginWithFacebook   = "user/fb_login"
    case Register			= "user/register"
    case UpdateProfile		= "update/profile"
	case AddPowerofAttorney	= "add/power_of_attorney"
    
    
    /****   Emergency Contact  APIS  *******/
    case GetEmergencycontact    = "list/emergency_contacts"
    case AddEmergencycontact    = "add/emergency_contact"
	case DeleteEmergencycontact = "emergency_contact/delete"
    case UpdateEmergencycontact = "emergency_contact/update"
	
	 /****   Get Power of Attorney APIS  *******/
    case GetPowerofAttorney     = "list/power_of_attorney"
	
    /****   Insurance APIS  *******/
    case AddInsurance       = "add/insurance"
    case GetInsurances      = "get/insurances"
    case DeleteInsurances   = "delete/insurance"
    case UpdateInsurances   = "update/insurance"
    
    /****   Medical History APIS  *******/
    case AddMedicalHistory      = "add/medical_history"
    case GetMedicalHistory      = "list/medical_history"
    case DeleteMedicalHistory   = "delete/medical_history"
	case UpdateMedicalHistory   = "update/medical_history"
	
	/****   Patient Will APIS  *******/
	case AddPatientWill      = "add/will"
	case GetPatientWill      = "get/will"
	case DeletePatientWill      = "delete/will"
	case UpdatePatientWill      = "update/will"
	
	/****   Patient Document APIS  *******/
	case AddPatientDocument      = "add/medical_document"
	case GetPatientDocument      = "get/medical_documents"
	case DeletePatientDocument   = "delete/medical_document"
	case UpdatePatientDocument   = "update/medical_document"
	
     /****   Private Doctor APIS  *******/
    case AddPrivateDoctor      = "add/doctor"
    case GetPrivateDoctor      = "list/doctors"
	
	
	/****   Search User APIS  *******/
	case SearchUserAPI		= "search/patient"
	case GetHospitals		= "get/hospitals"
	case GetDoctors			= "get/doctors"
	case UpdateChatID		= "update/chat_id"
    
    case GetPlaces    = "get/places"
    case CheckIn = "user/checkin"
    case RatePlace = "rate/place"
    case searchPlace = "search/places"
	
}

let isDevelopment = true

var kServerIPAddress: String {
    if isDevelopment {
		//		return "http://codingpixeldemo.com/link_merge/public/api/"
        return "http://139.162.37.73/link_merge/public/api/"
    }else{
        return "http://139.162.37.73/link_merge/public/api/"
		//		return "http://codingpixeldemo.com/link_merge/public/api/"
		
    }
}


var kBaseURLString: String {
    if isDevelopment {
        return "\(kServerIPAddress)"
    }else{
        return "\(kServerIPAddress)"
    }
}

var kBaseFileURLString: String {
    if isDevelopment {
        return "\(kServerIPAddress)public/profile_pics/"
    }else{
        return "\(kServerIPAddress)public/profile_pics/"
    }
}

let kFBUserGraphParams = ["fields": "id, name, first_name, last_name, picture.type(large), email"]

struct kEContact {
    
    static let ID           = "id"
    static let name         = "name"
    static let phone1       = "phone_1"
    static let phone2       = "phone_2"
    static let location     = "address"
    static let relation     = "contact_relation"
	static let isPowerofAttorney     = "is_power_attorney"
}

struct kPrivateDoctor {
    static let ID           = "id"
    static let name         = "name"
    static let contact      = "contact_number"
}

struct kPatientWill {
	
	static let ID           = "id"
	static let name         = "description"
	static let DocumentURL  = "documents"
}

struct kMedicalHistory {
    
    static let ID               = "id"
    static let name             = "name"
    static let illnssName       = "illness_name"
    static let illnssDetail     = "illness_detail"
    static let date             = "date"
    static let DocID            = "doctor_id"
    static let PrivateDoc       = "doctor"
	static let mHistoryID       = "medical_history_id"
}

struct kInsurance {
    
    static let ID               = "id"
    static let name             = "name"
    static let policyNumber     = "policy_number"
    static let carrierNumber    = "carrier_number"
    static let expiryDate       = "expiry_date"
    static let category         = "category"
    
}


struct kHospital {
	
	static let ID			= "id"
	static let name		= "name"
	static let area		= "area"
	static let distance	= "distance"
	static let lat		= "lat"
	static let lng		= "lng"
	
}



struct kDocument {
	static let ID					= "id"
	static let name				= "name"
	static let detail				= "detail"
	static let expiryDate			= "date"
	static let documnet_image		= "document"
	static let DocumentURL		= "document"
	static let MedicaldocumentID	= "medical_document_id"
	
	
}

