//
//  BaseViewController.swift
//  Wave
//
//  Created by Vengile on 07/06/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import ADEmailAndPassword
import MFSideMenu

class BaseViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
	
	let appdelegate = UIApplication.shared.delegate as! AppDelegate
	
	
	
//	let datePicker = SCPopDatePicker()
//	let PickerMain = SCPopPicker()
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	//MARK: Navigation Actions
	//MARK:
	func AddBackButton() {
		let navigation = self.navigationController as! BaseNavigationController
		navigation.addBackButtonOn(self, selector: #selector(BaseViewController.Back))
	}
	
	func addRightButton(selector: Selector , image : UIImage) {
//		let navigation = self.navigationController as! BaseNavigationController
//		navigation.addRightButton(self, selector: selector, image:image.imageResize(sizeChange: CGSize.init(width: 53, height: 25)))
	}
	
	func AddCrossButton(){
		let navigation = self.navigationController as! BaseNavigationController
		let image = UIImage.init(named: "Cross")
		navigation.addRLeftButton(self, selector: #selector(BaseViewController.Dismiss), image:image!.imageResize(sizeChange: CGSize.init(width: 53, height: 25)))

	}
	
	func Back(){
		_ = self.navigationController?.popViewController(animated: true)
	}
	
	func Dismiss(){
		self.dismiss(animated: true) {
			
		}
	}
	
	func addRightButtonWithtext(selector: Selector , lblText : String){
		
		let navigation = self.navigationController as! BaseNavigationController
		navigation.addRightButtonWithTitle(self, selector: selector, lblText: lblText)
	}
	
	func UpdateTitle(title : String) {
		let navigation = self.navigationController as! BaseNavigationController
		navigation.AddTitle(self, title: title)
	}
	
	func UpdateTitleOnSignUp(title : String) {
		let navigation = self.navigationController as! BaseNavigationController
		navigation.AddTitleOnSignUp(self, title: title)
	}
	
	func UpdateImageOnTitle() {
		let navigation = self.navigationController as! BaseNavigationController
		navigation.AddBackGroundImage()
	}
	
	func SetImageOnTitle() {
		let navigation = self.navigationController as! BaseNavigationController
		navigation.AddImageInTitle(self)
	}
	
	func AddMenuButton(selector: Selector) {
		let navigation = self.navigationController as! BaseNavigationController
		navigation.addMenuButtonOn(self, selector: selector)
	}
	
	
	
	
	func ShowPickerviewWithData(dataArray : [String]){
		
//		self.PickerMain.tapToDismiss = true
//		self.PickerMain.showBlur = true
//		self.PickerMain.arrayPicker = dataArray
//		
//		self.PickerMain.btnFontColour = UIColor.white
//		self.PickerMain.btnColour = UIColor.darkGray
//		self.PickerMain.showCornerRadius = true
//		
//		self.PickerMain.show(attachToView: self.view)
		
	}
	
	func ShowDatePicker(isSetMaxDate : Bool = false){
		
//		self.datePicker.tapToDismiss = true
//		self.datePicker.showBlur = true
//		self.datePicker.datePickerStartDate = Date()
//		self.datePicker.btnFontColour = UIColor.white
//		self.datePicker.btnColour = UIColor.darkGray
//		self.datePicker.showCornerRadius = true
//		
//		self.datePicker.show(attachToView: self.view ,SetMaxDate:  isSetMaxDate)
		
	}
	
	
	//MARK: Alert Messge
	//MARK:
	func ShowErrorAlert(message : String , AlertTitle : String = kErrorTitle) {
		let alert = UIAlertController(title: AlertTitle , message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: kOKBtnTitle, style: .default) { action in
			alert.dismiss(animated: true, completion: nil)
		})
		self.present(alert, animated: true, completion: nil)
	}
	
	func ShowSuccessAlert(message : String ) {
		let alert = UIAlertController(title: "" , message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: kOKBtnTitle, style: .default) { action in
			alert.dismiss(animated: true, completion: nil)
			self.navigationController?.popViewController(animated: true)
		})
		self.present(alert, animated: true, completion: nil)
	}
	
	
	func ShowSuccessAlertWithrootView(message : String ) {
		let alert = UIAlertController(title: "" , message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: kOKBtnTitle, style: .default) { action in
			alert.dismiss(animated: true, completion: nil)
			self.navigationController?.popToRootViewController(animated: true)
		})
		self.present(alert, animated: true, completion: nil)
	}
	
	func ShowSuccessAlertWithViewRemove(message : String ) {
		let alert = UIAlertController(title: "" , message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: kOKBtnTitle, style: .default) { action in
			alert.dismiss(animated: true, completion: nil)
			self.view.removeFromSuperview()
		})
		self.present(alert, animated: true, completion: nil)
	}
	
	func GetViewcontrollerWithName(nameViewController : String) -> UIViewController {
		let viewObj = (self.storyboard?.instantiateViewController(withIdentifier: nameViewController))! as UIViewController
		return viewObj
	}
	
	func PushViewWithIdentifier(name : String ) {
		let viewPush = self.storyboard?.instantiateViewController(withIdentifier: name)
		self.navigationController?.pushViewController(viewPush!, animated: true)
	}
	
	
	func PushViewLogin(ShowLoginView : Bool ) {
		let viewPush = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
		viewPush.ShowLogin = ShowLoginView
		self.navigationController?.pushViewController(viewPush, animated: true)
	}

	
	func ShowViewWithIdentifier(name : String ) {
		let viewPush = self.storyboard?.instantiateViewController(withIdentifier: name)
		self.present(viewPush!, animated: true) {
			
		}
	}

	
	// MARK:
	// MARK:SignIn Email Validation
	func EmailValidation(textField  : UITextField) -> Bool  {
		
       if textField.text?.characters.count == 0 {
            self.ShowErrorAlert(message: Alert.kEmptyCredentails ,AlertTitle: kInformationMissingTitle)
           return false
       }
       else {
        if ADEmailAndPassword.validateEmail(emailId: textField.text!) {
            return true
           }else {
              self.ShowErrorAlert(message: Alert.kWrongEmail)
               return false
           }
       }
	}
	
	// MARK:
	// MARK:Add Menu
	func ShowMenuBaseView(){
		
//		if DataManager.sharedInstance.user?.userType == UserType.FirstReponder.rawValue || DataManager.sharedInstance.user?.userType == UserType.Doctor.rawValue
//		{
//			
//			let menuView = self.storyboard?.instantiateViewController(withIdentifier: "FRMenuViewController") as! FRMenuViewController
//			
//			let mainFRView = self.storyboard?.instantiateViewController(withIdentifier: "FRMAinViewController") as! FRMAinViewController
//			
//			let navigationController = SaveMeNavigation(rootViewController: mainFRView)
//			//			let navigationController = UINavigationController(rootViewController: mainFRView)
//			navigationController.isNavigationBarHidden = true
//			let sideMenuViewController = AKSideMenu(contentViewController: navigationController, leftMenuViewController: menuView, rightMenuViewController: nil)
//			
//			sideMenuViewController.menuPreferredStatusBarStyle = .lightContent
//			sideMenuViewController.delegate = self
//			sideMenuViewController.contentViewShadowColor = .black
//			sideMenuViewController.contentViewShadowOffset = CGSize(width: 0, height: 0)
//			sideMenuViewController.contentViewShadowOpacity = 0.6
//			sideMenuViewController.contentViewShadowRadius = 12
//			sideMenuViewController.contentViewShadowEnabled = true
//			
//			self.present(sideMenuViewController, animated: true) {
//				self.navigationController?.popToRootViewController(animated: false)
//			}
//			
//		}else {
//			
//			var mainTabbar = self.storyboard?.instantiateViewController(withIdentifier: "MainTabbarViewController") as! UITabBarController
//			let menuView = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewViewController") as! MenuViewViewController
//			let imagename = "Tabbar3"
//			
//			//        if self.appdelegate.isPatient {
//			//            imagename = "CALL-911"
//			//        }
//			
//			mainTabbar = WSTabBarController(publishButtonConfig: {b in
//				b.setImage(UIImage(named: imagename), for: .normal)
//				b.setImage(UIImage(named: imagename), for: .highlighted)
//				b.frame = CGRect(x:0, y:0, width:64, height:64)
//				b.setTitle("", for: .normal)
//				b.contentVerticalAlignment = .top
//				b.titleEdgeInsets = UIEdgeInsets(top: 120, left: -64, bottom: 0 , right: 0)
//				b.titleLabel?.font = UIFont.systemFont(ofSize: 11)
//				
//				b.setTitleColor(UIColor.black, for: .normal)
//				b.setTitleColor(UIColor.black, for: .highlighted)
//				b.backgroundColor = UIColor.clear
//			}, publishButtonClick: { p in
//				self.ShowPopUpView()
//			})
//			mainTabbar.tabBar.isTranslucent = false;
//			mainTabbar.tabBar.tintColor =  UIColor.init(colorLiteralRed: 0.3294, green: 0.3961, blue: 0.8941, alpha: 1.0)
//			
//			mainTabbar.viewControllers = [self.GetViewcontrollerWithName(nameViewController: "Tabbar1"),
//			                              self.GetViewcontrollerWithName(nameViewController: "Tabbar2"),
//			                              self.GetViewcontrollerWithName(nameViewController: "Tabbar4"),
//			                              self.GetViewcontrollerWithName(nameViewController: "Tabbar5")
//			]
//			
//			// Create side menu controller
//			let navigationController = UINavigationController(rootViewController: mainTabbar)
//			navigationController.isNavigationBarHidden = true
//			let sideMenuViewController = AKSideMenu(contentViewController: navigationController, leftMenuViewController: menuView, rightMenuViewController: nil)
//			
//			sideMenuViewController.menuPreferredStatusBarStyle = .lightContent
//			sideMenuViewController.delegate = self
//			sideMenuViewController.contentViewShadowColor = .black
//			sideMenuViewController.contentViewShadowOffset = CGSize(width: 0, height: 0)
//			sideMenuViewController.contentViewShadowOpacity = 0.6
//			sideMenuViewController.contentViewShadowRadius = 12
//			sideMenuViewController.contentViewShadowEnabled = true
//			appdelegate.arrayViews.append(mainTabbar)
//			
//			self.present(sideMenuViewController, animated: true) {
//				self.navigationController?.popToRootViewController(animated: false)
//			}
//			
//		}
		
		
	}
	
	// MARK:
	// MARK: - <AKSideMenuDelegate>
//	open func sideMenu(_ sideMenu: AKSideMenu, willShowMenuViewController menuViewController: UIViewController) {
//		print("willShowMenuViewController")
//	}
//	
//	open func sideMenu(_ sideMenu: AKSideMenu, didShowMenuViewController menuViewController: UIViewController) {
//		print("didShowMenuViewController")
//	}
//	
//	open func sideMenu(_ sideMenu: AKSideMenu, willHideMenuViewController menuViewController: UIViewController) {
//		print("willHideMenuViewController")
//	}
//	
//	open func sideMenu(_ sideMenu: AKSideMenu, didHideMenuViewController menuViewController: UIViewController) {
//		print("didHideMenuViewController")
//	}
//	
	
	
	
	
	//MARK:- Custom methods
    func showLoading() {
        self.view.showLoading()
    }
//
    func hideLoading() {
        self.view.hideLoading()
    }
	
	//MARK: Show Media Options
	//MARK:
	func showMediaChoosingOptions() {
		let imagePicker = UIImagePickerController()
		imagePicker.delegate = self
		imagePicker.allowsEditing = true
		let photoOptionMenu = UIAlertController(title: "Choose source", message: kEmptyString, preferredStyle: .actionSheet)
		let libraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
			(alert: UIAlertAction!) -> Void in
			imagePicker.sourceType = .photoLibrary
			//            imagePicker.mediaTypes =  kUTTypeImage as! [String]
			
			self.present(imagePicker, animated: true, completion: nil)
		})
		let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
			(alert: UIAlertAction!) -> Void in
			if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
				imagePicker.sourceType = .camera
				//                imagePicker.mediaTypes =  kUTTypeImage as! [String]
				self.present(imagePicker, animated: true, completion: nil)
			}
			else {
				self.ShowErrorAlert(message:"Your camera is not accessible. Please check your device settings and then try again.")
			}
		})
		let cancelAction = UIAlertAction(title: kCancelBtnTitle, style: .cancel, handler: {
			(alert: UIAlertAction!) -> Void in
		})
		photoOptionMenu.addAction(libraryAction)
		photoOptionMenu.addAction(cameraAction)
		photoOptionMenu.addAction(cancelAction)
		self.present(photoOptionMenu, animated: true, completion: nil)
	}
	
	
	
	func GetEmptyView(viewP : UIView)-> UILabel{
		let messageLabel = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: viewP.bounds.size.width, height: viewP.bounds.size.height))
		messageLabel.text = "No Record Found"
		messageLabel.textColor = UIColor.black
		messageLabel.numberOfLines = 0;
		messageLabel.textAlignment = .center;
		messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
		messageLabel.sizeToFit()
		
		return messageLabel
	}
    
    func showTabbar() {
        let tabbar = storyboard?.instantiateViewController(withIdentifier: "WaveTabbarViewController")
        let leftMenu = storyboard?.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        let mfContainer = storyboard?.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
        
        mfContainer.centerViewController = tabbar
        mfContainer.leftMenuViewController = leftMenu
        mfContainer.leftMenuWidth = view.frame.size.width - 100
        mfContainer.panMode = .init(0)
        
        present(mfContainer, animated: true, completion: nil)
    }
    
    
}
