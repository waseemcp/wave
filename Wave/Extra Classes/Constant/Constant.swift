//
//  Constant.swift
//  Shir
//
//  Created by waseem shah on 2/27/17.
//  Copyright © 2017 waseem shah. All rights reserved.
//

import Foundation
import UIKit

let kEmptyString = ""


struct Constants {
    // Constants to call services to server
    static let kPurpleColor = UIColor.init(colorLiteralRed: 0.5216, green: 0.6275, blue: 0.9451, alpha: 1.0)
    static let kBlackColor = UIColor.init(colorLiteralRed: 0.6901, green: 0.6901, blue: 0.6901, alpha: 1.0)
	
	static let kNavigationcolor = UIColor.init(colorLiteralRed: 0.1647, green: 0.5882, blue: 0.9176, alpha: 1.0)
	
    static let kPlaceholderColor = UIColor.gray
    
    static let kCornerRaious : CGFloat = 5.0
    
    
    static let cos45: CGFloat = 0.70710678118
    
    static var deviceToken = "DummyToken"
    
    static let kYellowColor = UIColor(red:0.99, green:0.74, blue:0.05, alpha:1.0)
    static let kSilverColor = UIColor(red:0.77, green:0.82, blue:0.87, alpha:1.0)    
}


//MARK:- Numbers
let kZero: CGFloat = 0.0
let kOne: CGFloat = 1.0
let kHalf: CGFloat = 0.5
let kTwo: CGFloat = 2.0
let kHundred: CGFloat = 100.0
let kDefaultAnimationDuration = 0.3



//MARK:- Messages

struct Alert {
    
    static let kEmptyCredentails    = "Please provide your credentails to proceed."
    static let kWrongEmail          = "Incorrect email format. Please provide a valid email address."
	static let kEmptyEmail          = "Email address is missing."
	static let kEmptyPassword       = "Password is missing."
	static let kPasswordNotMatch    = "Password not match with confirm password."
	static let kUserNameMissing     = "UserName is missing."
	static let kUserImageMissing     = "User image is missing."
	
	static let kBloodTypeMissing		= "Choose Blood Type."
	static let kAgeMissing			= "Age missing."
	static let kGenderMissing			= "Gender missing."
	static let kDLMissing				= "DL Number missing."
	static let KSSNInvalid			= "SSN missing or invalid(9 char. required)."
	static let kLocationMissing		= "Location missing"
	static let kStateMissing			= "Choose state."
	
	static let kDoctorNameMissing		= "Doctor name missing."
	static let kDoctorPhoneMissing	= "Doctor contact number missing."
	
	static let kMHistoryIllnessMissing	= "Illness name missing."
	static let kDateMissing			= "Date missing."
	static let kDetailMissing			= "Enter some description."
	
	static let kNameMissing			= "Name missing."
	static let kRelationMissing		= "Relationship missing."
	static let kPhoneMissing			= "Phone missing."
	static let kCarrierMissing			= "Carrier missing."
	static let kPolicyNumberMissing	= "Policy number missing."
	static let kCategoryMissing		= "Category missing."
	static let kDiscriptionMissing		= "Discription missing."
	static let kImageMissing			= "Image missing."
}


let kErrorTitle				= "Error"
let kInformationMissingTitle = "Information Missing"
let kOKBtnTitle				= "OK"
let kCancelBtnTitle			= "Cancel"
let kDismissBtnTitle			= "Dismiss"
let kTryAgainBtnTitle		= "Try Again"
let kChooseBloodType			= "Bood Type"
let kChooseGender			= "Gender"
let kChooseState				= "Choose State"
let kChooseAge				= "Age"

let kDescriptionHere = "Description Here..."
let kDoctorName		= "Doctor Name"
let kDate			= "Date"


var kNetworkNotAvailableMessage = "It appears that you are not connected with internet. Please check your internet connection and try again."
var kServerNotReachableMessage = "We are unable to connect our server at the moment. Please check your device internet settings or try later."
var kFacebookSigninFailedMessage = "We are unable to get your identity from Facebook. Please check your Facebook profile settings and then try again."


var prfileArray = [["" : ""], ["" : ""], ["name" : "Patients Will" , "image" : "Will"],["name" : "Previous Doctor" , "image" : "HeartBlue"],["name" : "Insurance" , "image" : "Insurance"]]

var patientArray = [["" : ""], ["" : ""], ["name" : "Patients Will" , "image" : "Will"],["name" : "Previous Doctor" , "image" : "HeartBlue"],["name" : "Insurance" , "image" : "Insurance"],["name" : "Emergency Contact" , "image" : "Tabbar5S"],["name" : "Medical History" , "image" : "Tabbar2S"],["name" : "Medical Document" , "image" : "Tabbar4S"]]

