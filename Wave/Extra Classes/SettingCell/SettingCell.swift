//
//  SettingCell.swift
//  Wave
//
//  Created by Vengile on 7/12/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import WOWMarkSlider
class SettingCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var toggleSwitch: UISwitch!
  
    @IBOutlet weak var slider: WOWMarkSlider!
    
    @IBOutlet weak var sliderValueLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var maleImage: UIImageView!
    @IBOutlet weak var femaleImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    @IBAction func sliderChangeAction(_ sender: UISlider) {
        sliderValueLabel.text = "\(Int(sender.value))"
        UserDefaults.standard.set(Float(sender.value), forKey: "radius")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func maleButtonAction() {
        maleImage.image = UIImage.init(named: "RadioS")
        femaleImage.image = UIImage.init(named: "Radio")
        
        let params = ["gender": "male"] as [String: Any]
        
        APIWrapper.postRequest(with: "update/profile", params: params, image: nil) { (response) in
//            Utilities.showAlert(with: "Success", message: "Name is changed")
            WaveUser.shared.gender = "male"
            
            DataManager.sharedInstance.user?.userName = WaveUser.shared.name!
            DataManager.sharedInstance.saveUserPermanentally()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMenu"), object: nil)
            
            
        }
        
    }
    
    @IBAction func femaleButtonAction() {
        maleImage.image = UIImage.init(named: "Radio")
        femaleImage.image = UIImage.init(named: "RadioS")
        
        
        let params = ["gender": "female"] as [String: Any]
        
        APIWrapper.postRequest(with: "update/profile", params: params, image: nil) { (response) in
//            Utilities.showAlert(with: "Success", message: "Name is changed")
            WaveUser.shared.gender = "female"
            
            
            DataManager.sharedInstance.user?.userName = WaveUser.shared.name!
            DataManager.sharedInstance.saveUserPermanentally()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMenu"), object: nil)
            
            
        }
    }
    
    
    
}
