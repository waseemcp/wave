//
//  ForgotPasswordViewController.swift
//  Wave
//
//  Created by Jawad Ahmed on 12/16/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var emailTexrField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    
    // MARK: - IBActions
    
    @IBAction func signInAction() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func sendEmailAction() {
        sendEmail()
    }
    
    
    
    
    // MARK: - API MEthods
    
    func sendEmail() {
        let params = ["email": emailTexrField.text!] as [String: Any]
        
        APIWrapper.postRequest(with: "forgot/password", params: params, image: nil) { (response) in
            Utilities.showAlert(with: "Success", message: "Email is sent")
        }
    }
    
    
    
    
    // MARK: - Text Field Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
