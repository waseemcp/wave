//
//  HelpPagesViewController.swift
//  Wave
//
//  Created by Vengile on 05/07/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class HelpPagesViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet var scroller: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    
    var scrollPagesLoaded = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
             NotificationCenter.default.addObserver(self, selector: #selector(self.PushMainView), name: NSNotification.Name(rawValue: "PushMainView"), object: nil)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        if (scrollPagesLoaded == false) {
            scrollPagesLoaded = true
            
            let tutorialsXibs = Bundle.main.loadNibNamed("TutorialViews", owner: self, options: nil)
            for i in 0...1 {
                let view = tutorialsXibs?[i] as! TutorialViews
                view.frame = CGRect.init(x: CGFloat(i)*scroller.frame.size.width, y: 0, width: scroller.frame.size.width, height: scroller.frame.height)
                scroller.addSubview(view)
            }
            
            scroller.contentSize = CGSize.init(width: scroller.frame.size.width * 2, height: scroller.frame.size.height)
        }
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    func PushMainView(){
        self.showTabbar()
        _ = self.navigationController?.popToRootViewController(animated: true)
        
    }
}
