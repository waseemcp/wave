//
//  SignInViewController.swift
//  Wave
//
//  Created by Vengile on 08/06/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import MFSideMenu
import FBSDKLoginKit
import FBSDKCoreKit
import FacebookCore
import FacebookLogin
import IQKeyboardManager
class SignInViewController: BaseViewController {

	
	@IBOutlet var Btn_SignIn :UIButton!
	@IBOutlet var Btn_SignUp :UIButton!
	
	
	@IBOutlet var View_SignUp : UIView!
	
	@IBOutlet var TF_SignUP_Name : UITextField!
	@IBOutlet var TF_SignUP_Password : UITextField!
	@IBOutlet var TF_SignUP_Email : UITextField!
	@IBOutlet var TF_SignUP_Age : UITextField!

	@IBOutlet var TF_SignIn_Email : UITextField!
	@IBOutlet var TF_SignIn_Password : UITextField!
		
	@IBOutlet var Img_Male : UIImageView!
	@IBOutlet var Img_FeMale : UIImageView!
    
    @IBOutlet var Btn_SignUP_Camera : UIButton!
	
    @IBOutlet var imgView_Profile          : UIImageView!
    
	var ShowLogin = true
    var genderString = "female"
    var userImage: UIImage?
    var returnKeyHandler:IQKeyboardReturnKeyHandler?
	
    override func viewDidLoad() {
        super.viewDidLoad()
      returnKeyHandler = IQKeyboardReturnKeyHandler(viewController: self)
        // Do any additional setup after loading the view.
        self.View_SignUp.isHidden = true
        
        if !ShowLogin {
            self.showSignup()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	override func viewWillAppear(_ animated: Bool) {
		
         UIApplication.shared.isStatusBarHidden = true
		
        
//        TF_SignIn_Email.text = "z@z.com"
//        TF_SignIn_Password.text = "123456"
        
        
		
	}
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
         UIApplication.shared.isStatusBarHidden = false
    }
    
    // MARK: - Class Methods
    
   
    

	//MARK: Sign In Action
	//MARK:
	@IBAction func SignInAction(sender : UIButton){
		self.EmptyTextFields()
        self.view.endEditing(true)
		self.Btn_SignIn.setTitleColor(UIColor.white, for: UIControlState.normal)
		self.Btn_SignUp.setTitleColor(UIColor.gray, for: UIControlState.normal)
		self.View_SignUp.isHidden = true

	}
	
	@IBAction func SignUpAction(sender : UIButton){
		self.EmptyTextFields()
        self.view.endEditing(true)
		self.Btn_SignUp.setTitleColor(UIColor.white, for: UIControlState.normal)
		self.Btn_SignIn.setTitleColor(UIColor.gray, for: UIControlState.normal)
		self.View_SignUp.isHidden = false
	}
    func showSignup(){
        self.Btn_SignUp.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.Btn_SignIn.setTitleColor(UIColor.gray, for: UIControlState.normal)
        self.View_SignUp.isHidden = false
    }
}

extension SignInViewController {

    @IBAction func CreateAccount_Action(sender : UIButton){
        //        self.GotoHelpPages()
        
        let params = ["name": TF_SignUP_Name.text!,
                      "gender": genderString,
                      "age": TF_SignUP_Age.text!,
                      "email": TF_SignUP_Email.text!,
                      "password": TF_SignUP_Password.text!,
                      "device_id": UIDevice.current.identifierForVendor?.uuidString ?? "",
                      "device_type": "ios"] as [String: Any]
        
        APIWrapper.postRequest(with: "user/register", params: params, image: imgView_Profile.image) { (response) in
            WaveUser.shared.create(with: response as! [String: Any])

            self.GotoHelpPages()
        }
    }
	
	
	@IBAction func Login_Action(sender : UIButton){
		//self.GotoHelpPages()
        
        
        
        let params = ["email": TF_SignIn_Email.text!,
                      "password": TF_SignIn_Password.text!] as [String: Any]

        APIWrapper.postRequest(with: "user/login", params: params, image: nil) { (response) in
            WaveUser.shared.create(with: response as! [String: Any])
            
//            self.showTabbar()
            self.GotoHelpPages()
        }
	}
	
	
	func GotoHelpPages(){
		self.PushViewWithIdentifier(name: "HelpPagesViewController")
	}
    func CheckValidation() -> Bool{
        var returnVaue = true
         if self.TF_SignUP_Name.text?.characters.count == 0 {
            self.ShowErrorAlert(message: "Name missing")
            returnVaue = false
        }
         else if self.TF_SignUP_Age.text?.characters.count == 0 {
            self.ShowErrorAlert(message: "Age missing")
            returnVaue = false
         }
        else if self.TF_SignUP_Email.text?.characters.count == 0 {
            self.ShowErrorAlert(message: "Email address missing")
            returnVaue = false
        }else if !self.EmailValidation(textField: self.TF_SignUP_Email) {
            returnVaue = false
        }else if self.TF_SignUP_Password.text?.characters.count == 0 {
            self.ShowErrorAlert(message: "Password missing")
            returnVaue = false
        }
        
        return returnVaue
    }
}


extension SignInViewController {
	func EmptyTextFields(){
		self.TF_SignUP_Age.text = ""
		self.TF_SignUP_Password.text = ""
		self.TF_SignUP_Name.text = ""
		self.TF_SignUP_Email.text = ""
		self.TF_SignIn_Email.text = ""
		self.TF_SignIn_Password.text = ""
	}
    
    @IBAction func cameraAction() {
        let picker = UIImagePickerController()
        picker.delegate = self
        
        let alert = UIAlertController(title: "Choose image" , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
             picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Library", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            alert.dismiss(animated: true, completion: nil)
            
        })
        
        
        
        
        present(alert, animated: true, completion: nil)
    }
    
	@IBAction func MaleBtn_Action(sender : UIButton){
		Img_Male.image = UIImage.init(named: "RadioS")
		Img_FeMale.image = UIImage.init(named: "Radio")
        genderString = "male"
	}
	
	@IBAction func FeMaleBtn_Action(sender : UIButton){
		Img_Male.image = UIImage.init(named: "Radio")
		Img_FeMale.image = UIImage.init(named: "RadioS")
        genderString = "female"
	}
    @IBAction func FacebookLogin(sender : UIButton){
        self.loginWithFacebook()
    }
    @IBAction func FacebookLogin2(sender : UIButton){
        self.loginWithFacebook()
    }
    
}
extension SignInViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imgView_Profile.image = info[UIImagePickerControllerOriginalImage] as! UIImage?
      
        picker.dismiss(animated: true, completion: nil)
    }
    func loginWithFacebook() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions:[ .publicProfile,.email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
                
            case .cancelled:
                print("User cancelled login.")
            case .success(grantedPermissions: let grantedPermissions, declinedPermissions: let declinedPermissions, token: let accessToken):
                let params: [String : Any]? = ["fields": "id, name, email"]
                
                let graphRequest = GraphRequest(graphPath: "/me", parameters: params!)
                graphRequest.start {
                    (urlResponse, requestResult) in
                    
                    switch requestResult {
                    case .failed(let error):
                        print("error in graph request:", error)
                        break
                    case .success(let graphResponse):
                        if let responseDictionary = graphResponse.dictionaryValue {
                            print(responseDictionary)
                            self.showLoading()
                            
                            let user = User()
                            
                            user.userName = responseDictionary["name"] as! String
                            user.FBID = responseDictionary["id"] as! String
                            user.email = responseDictionary["email"] as! String
                            let pictureUrlString = "https://graph.facebook.com/\(user.FBID)/picture?type=large&return_ssl_resources=1"
                            let pictureUrl = URL(string: pictureUrlString)
                            let data = try? Data(contentsOf: pictureUrl!)
                            
                            if data != nil {
                                let image = UIImage(data: data!)
                                user.profileImage = image
                            }
                            
                            user.deviceID = "Dummy token"
                            user.deviceType = "ios"
                            user.age = 0
                            
                            user.gender = "male"
                            NetworkManager.register(user: user, completion: { (success, message, updatedUser,response) in
                                self.hideLoading()
                                if success {
                                    WaveUser.shared.create(with: response as [String: Any])
                                    DataManager.sharedInstance.user = updatedUser
                                    DataManager.sharedInstance.saveUserPermanentally()
                                    self.GotoHelpPages()
                                }else {
                                    self.ShowErrorAlert(message: message)
                                }
                            })
                            
                            
                        }
                    }
                }
                print ("Logged In")
            }
        }
        
    }
    
}

