//
//  MapViewController.swift
//  Wave
//
//  Created by Vengile on 7/11/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import MFSideMenu
import MapKit
import WOWMarkSlider

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var radiusLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var radiusSlider: WOWMarkSlider!
    var gotLocation = false
    var placesArray = [Location]()
    var placesArrayToPass = [[String:Any]]()
    var IndexOfAnnotation:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Discover"
        
        searchTextField.attributedPlaceholder = NSAttributedString.init(string: searchTextField.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        let manager = (UIApplication.shared.delegate as? AppDelegate)?.locationManager
        manager?.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            manager?.startUpdatingLocation()
        }
        if ((UserDefaults.standard.value(forKey: "radius")) != nil) {
            if let radiusValue = UserDefaults.standard.value(forKey: "radius"){
                let radius = Int(radiusValue as! Float)
                radiusLabel.text = "\(radius)"
                radiusSlider.setValue(radiusValue as! Float, animated: true)
            }
        }
        let nc = NotificationCenter.default // Note that default is now a property, not a method call
        nc.addObserver(forName:Notification.Name(rawValue:"showDetail"),
                       object:nil, queue:nil) {
                        notification in
                        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "PlaceDetailViewController") as! PlaceDetailViewController
                        detailVC.placeData = self.placesArrayToPass[self.IndexOfAnnotation!]
                        self.navigationController?.pushViewController(detailVC, animated: true)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        searchTextField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        (navigationController as! BaseNavigationController).addLeftMenuButtonOn(self, selector: #selector(menuAction))
        (navigationController as! BaseNavigationController).addProfileCountViewOn(self, selector: #selector(profileCountViewAction))
        
        
        if ((WaveUser.shared.location?.coordinate.latitude) != nil){
            self.getPlaces()
        }
        
    }
    
    // MARK: - IBActions
    
    @IBAction func menuAction() {
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @IBAction func profileCountViewAction() {
        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func sliderChangeAction(_ sender: UISlider) {
        radiusLabel.text = "\(Int(sender.value))"
        UserDefaults.standard.set(Float(sender.value), forKey: "radius")
        UserDefaults.standard.synchronize()
        self.zoomMap(byFactor: Double(sender.value))
    }
    
    
    func zoomMap(byFactor delta: Double) {
        let miles = delta
        let delta = miles / 10.0
        
        var currentRegion = self.mapView.region
        currentRegion.span = MKCoordinateSpan(latitudeDelta: delta, longitudeDelta: delta)
        self.mapView.region = currentRegion
     
  
    }
    
    // MARK: - API Methods
    
    func getPlaces() {
        let params = ["radius": radiusLabel.text!,
                      "lat": (WaveUser.shared.location?.coordinate.latitude)!,
                      "lon": (WaveUser.shared.location?.coordinate.longitude)!
            ] as [String: Any]
        
        APIWrapper.getRequest(with: "get/places", params: params) { (response) in
            let dataDict = response as! [[String : Any]]
            self.placesArrayToPass = dataDict
             self.placesArray.removeAll()
            for dataValue in dataDict {
                self.placesArray.append(Location(json:(dataValue as? [String : Any]) as [String : AnyObject]?))
            }
            
            
            
            self.DropPins()
        }
    }
    
    
    func searchPlaces(with searchText: String) {
        let params = ["lat": (WaveUser.shared.location?.coordinate.latitude)!,
                      "lon": (WaveUser.shared.location?.coordinate.longitude)!,
                      "place_name": searchText,
                      "radius": radiusLabel.text!] as [String: Any]
        
        APIWrapper.getRequest(with: "search/places", params: params) { (response) in
            let dataDict = response as! [[String : Any]]
            self.placesArrayToPass = dataDict
            self.placesArray.removeAll()
            for dataValue in dataDict {
                self.placesArray.append(Location(json:(dataValue as? [String : Any]) as [String : AnyObject]?))
            }
            
          
            
            self.DropPins()
            //self.appdelegate.locationArray = self.placesArray
        }
    }
    
    func DropPins() {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)

        for dataValue in self.placesArray
            
        {
            
            print(dataValue.lon)
            print(dataValue.lat)
            print(dataValue.name)
            
            if dataValue.lat.characters.count > 0 && dataValue.lon.characters.count > 0 {
                
                
                
                if Double(dataValue.lat)! != 0 && Double(dataValue.lon)! != 0 {
                    
                    let coordinate = CLLocationCoordinate2D.init(latitude: Double(dataValue.lat)!, longitude: Double(dataValue.lon)!)
                    let annotation = CustomAnnot(title: dataValue.name, coordinate: coordinate, location: dataValue)
                 
                    
                    self.mapView.addAnnotation(annotation)
                    
                    
                    //                    let mappin = PinAnnotation(title: dataValue.name, coordinate: CLLocationCoordinate2D(latitude: Double(dataValue.lat)!, longitude:Double(dataValue.lon)! ), info: "")
                    //                    mapView.addAnnotation(mappin)
                }
                
            }
        }
        
    }
    
    
    // MARK: - TextField Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchPlaces(with: textField.text!)
        
        textField.resignFirstResponder()
        return true
    }
    
    
    
    // MARK: - LocationManager Delegates
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (gotLocation == false) {
            gotLocation = true
            
            WaveUser.shared.location = locations.last
            manager.stopUpdatingLocation()
            
            getPlaces()
        }
    }
    
    
    // MARK: - MapView Delegates
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotaionView = mapView.dequeueReusableAnnotationView(withIdentifier: "FrogAnnotation")
        if annotaionView == nil {
            annotaionView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: "FrogAnnotation")
        }
        
        annotaionView?.image = UIImage(named: "ping_1")
        return annotaionView!
    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation as? CustomAnnot {
        IndexOfAnnotation = self.placesArray.index(of: annotation.location)
            PinView.loadFromXib().addInView(parentView: self.view,annotation: annotation)
            
            if (mapView.selectedAnnotations.count > 0) {
                self.mapView.selectedAnnotations.removeAll()
            }
        }
       
    }
    
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
    }
    
    @IBAction func ClearSearch(sender : UIButton){
        
        if searchTextField.hasText {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            self.getPlaces()
        }
        
    }
}
