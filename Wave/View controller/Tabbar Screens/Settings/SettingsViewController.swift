//
//  SettingsViewController.swift
//  Wave
//
//  Created by Vengile on 7/11/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Settings"
        (navigationController as! BaseNavigationController).addLeftMenuButtonOn(self, selector: #selector(menuAction))
    }
    
    
    func ShowChangeAge() {
        let alert = UIAlertController.init(title: "Change Age", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Age"
            textField.keyboardType = .numberPad
            textField.isSecureTextEntry = false
        }
        
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Save", style: .default, handler: { (action) in
            
            self.changeAge(newAge: alert.textFields?.first?.text)
        }))
        
        alert.show()
    }
    
    

    
    func ShowChangeName() {
        let alert = UIAlertController.init(title: "Change Name", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Name"
            textField.keyboardType = .default
            textField.isSecureTextEntry = false
        }
        
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Save", style: .default, handler: { (action) in
            
            self.changeName(newName: alert.textFields?.first?.text)
        }))
        
        alert.show()
    }
    

    
    
    func showChangePasswordAlert() {
        let alert = UIAlertController.init(title: "Change Password", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Old Password"
            textField.isSecureTextEntry = true
        }
        
        alert.addTextField { (textField) in
            textField.placeholder = "New Password"
            textField.isSecureTextEntry = true
        }
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction.init(title: "Save", style: .default, handler: { (action) in
            self.changePassword(oldPass: alert.textFields?.first?.text, newPass: alert.textFields?.last?.text)
        }))
        
        alert.show()
    }
    
    
    
    
    // MARK: - IBActions
    
    @IBAction func menuAction() {
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    
    
    
    // MARK: - API MEthods
    
    func changePassword(oldPass: String?, newPass: String?) {
        let params = ["old_password": oldPass!,
                      "changed_password": newPass!] as [String: Any]
        
        APIWrapper.postRequest(with: "change/password", params: params, image: nil) { (response) in
            Utilities.showAlert(with: "Success", message: "Passwrod is changed")
        }
    }
    
    
    
    func changeAge( newAge: String?) {
        let params = ["age": newAge!] as [String: Any]
        
        APIWrapper.postRequest(with: "update/profile", params: params, image: nil) { (response) in
            Utilities.showAlert(with: "Success", message: "Age is changed")
            WaveUser.shared.age = Int(newAge!)
            
            self.table.reloadData()
            DataManager.sharedInstance.user?.age = WaveUser.shared.age
            DataManager.sharedInstance.saveUserPermanentally()
            
        }
    }
    
    
    func changeName( newName: String?) {
        let params = ["name": newName!] as [String: Any]
        
        APIWrapper.postRequest(with: "update/profile", params: params, image: nil) { (response) in
            Utilities.showAlert(with: "Success", message: "Name is changed")
            WaveUser.shared.name = newName
            
            self.table.reloadData()
            DataManager.sharedInstance.user?.userName = WaveUser.shared.name!
            DataManager.sharedInstance.saveUserPermanentally()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMenu"), object: nil)

            
        }
    }
    
    
    func ChangeImage(imageMain : UIImage){
        let params = ["":""] as [String: Any]
        
        APIWrapper.postRequest(with: "update/profile", params: params, image: imageMain) { (response) in
            
            print(response)
            if let newData = (response as! [String : Any])["user_image"] as? String {
                WaveUser.shared.imagepath = newData
                DataManager.sharedInstance.user?.imagepath = WaveUser.shared.imagepath!
                DataManager.sharedInstance.saveUserPermanentally()
            }
            
            
            
            Utilities.showAlert(with: "Success", message: "Image is changed")
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RefreshMenu"), object: nil)

            
        }
    }
    
    // MARK: - TableView DataSource & Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 50.0))
        label.backgroundColor = UIColor(red:0.17, green:0.19, blue:0.23, alpha:1.0)
        label.font = UIFont.init(name: "MyriadPro-Regular", size: 14.0)!
        label.textColor = UIColor.white
        label.textAlignment = .center
        
        if section == 0 {
            label.text = "GENERAL SETTINGS"
        } else if section == 1 {
            label.text = "PROFILE SETTINGS"
        } else {
            label.text = "SUPPORT"
        }
        
        return label
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        } else if section == 1 {
            return 5
        }
        else {
            return 4
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: SettingCell?
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellSwitch") as? SettingCell
                cell?.titleLabel.text = "Send Push Notifications"
            }
            else if indexPath.row == 1 {
                cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellSlider") as? SettingCell
                if ((UserDefaults.standard.value(forKey: "radius")) != nil) {
                    if let radiusValue = UserDefaults.standard.value(forKey: "radius"){
                        var radius = Int(radiusValue as! Float)
                        cell?.sliderValueLabel.text = "\(radius as! Int)"
                        cell?.slider.setValue(radiusValue as! Float, animated: true)
                    }
                }
                cell?.titleLabel.text = "Set Radius Limit"
            }
        }
        else if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellValue") as? SettingCell
                cell?.titleLabel.text = "Your Name"
                cell?.valueLabel.text = WaveUser.shared.name
            }
            else if indexPath.row == 1 {
                cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellRadio") as? SettingCell
                cell?.titleLabel.text = "Gender"
                if WaveUser.shared.gender == "male" {
                    cell?.maleButtonAction()
                }
                else {
                    cell?.femaleButtonAction()
                }
            }
            else if indexPath.row == 2 {
                cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellValue") as? SettingCell
                cell?.titleLabel.text = "Age"
                
                if let age = WaveUser.shared.age {
                 cell?.valueLabel.text = String.init(describing: WaveUser.shared.age!)
                }
                else {
                    cell?.valueLabel.text = ""
                }
            }
            else {
                cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellDefault") as? SettingCell
                
                if indexPath.row == 3 {
                    cell?.titleLabel.text = "Change Profile Picture"
                    cell?.accessoryType = .disclosureIndicator;
                } else {
                    cell?.titleLabel.text = "Change Password"
                    cell?.accessoryType = .disclosureIndicator;
                }
            }
        }
        else {
            cell = tableView.dequeueReusableCell(withIdentifier: "SettingCellDefault") as? SettingCell
            
            if indexPath.row == 0 {
                cell?.titleLabel.text = "Support"
                cell?.accessoryType = .disclosureIndicator;
            } else if indexPath.row == 1 {
                cell?.titleLabel.text = "About the Wave App"
                cell?.accessoryType = .disclosureIndicator;
            } else if indexPath.row == 2 {
                cell?.titleLabel.text = "Privacy"
                cell?.accessoryType = .disclosureIndicator;
            } else {
                cell?.titleLabel.text = "Legal"
                cell?.accessoryType = .disclosureIndicator;
            }
        }
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 0 {
                self.ShowChangeName()
            }else
            if indexPath.row == 2 {
                self.ShowChangeAge()
            }else if indexPath.row == 3 {
             self.ShowImage()
            }
            else if indexPath.row == 4 {
                showChangePasswordAlert()
            }
        }
    }
    
    
    func ShowImage(){
        let picker = UIImagePickerController()
        picker.delegate = self
        
        let alert = UIAlertController(title: "Choose image" , message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
            picker.sourceType = .camera
            self.present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Library", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
            picker.sourceType = .photoLibrary
            self.present(picker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            alert.dismiss(animated: true, completion: nil)
            
        })
        
        
        
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.ChangeImage(imageMain: (info[UIImagePickerControllerOriginalImage] as! UIImage?)!)
        
        picker.dismiss(animated: true, completion: nil)
    }
}
