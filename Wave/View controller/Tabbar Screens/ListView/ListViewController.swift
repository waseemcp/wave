//
//  ListViewController.swift
//  Wave
//
//  Created by Vengile on 7/11/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import WOWMarkSlider
class ListViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var radiusLabel: UILabel!
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var radiusSlider: WOWMarkSlider!
    var placesArray = [[String: Any]]()
    
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Discover"
        if ((UserDefaults.standard.value(forKey: "radius")) != nil) {
            if let radiusValue = UserDefaults.standard.value(forKey: "radius"){
                var radius = Int(radiusValue as! Float)
                radiusLabel.text = "\(radius as! Int)"
                radiusSlider.setValue(radiusValue as! Float, animated: true)
            }
        }

        searchTextField.attributedPlaceholder = NSAttributedString.init(string: searchTextField.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        
        refreshControl = UIRefreshControl()
//        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        table.addSubview(refreshControl)
        table.backgroundColor = UIColor.clear
    }
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        
        refreshControl.endRefreshing()
        getPlaces()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        searchTextField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        (navigationController as! BaseNavigationController).addLeftMenuButtonOn(self, selector: #selector(menuAction))
        (navigationController as! BaseNavigationController).addProfileCountViewOn(self, selector: #selector(profileCountViewAction))
        
        getPlaces()
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func menuAction() {
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @IBAction func profileCountViewAction() {
        let notificationVC = storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func sliderChangeAction(_ sender: UISlider) {
        radiusLabel.text = "\(Int(sender.value))"
        UserDefaults.standard.set(Float(sender.value), forKey: "radius")
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - API Methods
    
    func getPlaces() {
        let params = ["radius": radiusLabel.text!,
                      "lat": (WaveUser.shared.location?.coordinate.latitude)!,
                      "lon": (WaveUser.shared.location?.coordinate.longitude)!
                    ] as [String: Any]
        
        APIWrapper.getRequest(with: "get/places", params: params) { (response) in
            self.placesArray = response as! [[String: Any]]
            self.table.reloadData()
        }
    }
    
    
    func searchPlaces(with searchText: String) {
        let params = ["lat": (WaveUser.shared.location?.coordinate.latitude)!,
                      "lon": (WaveUser.shared.location?.coordinate.longitude)!,
                      "place_name": searchText,
                      "radius": radiusLabel.text!] as [String: Any]
        
        APIWrapper.getRequest(with: "search/places", params: params) { (response) in
            self.placesArray = response as! [[String: Any]]
            self.table.reloadData()
        }
    }
    
    
    
    
    // MARK: - TextField Delegates
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchPlaces(with: textField.text!)
        
        textField.resignFirstResponder()
        return true
    }
    
    
    
    // MARK: - TableView DataSource & Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesArray.count
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ListViewCell") as? ListViewCell
        if (cell == nil) {
            cell = Bundle.main.loadNibNamed("ListViewCell", owner: self, options: nil)?[0] as? ListViewCell
        }
        
        cell?.updateWithInfo(placesArray[indexPath.row])
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = storyboard?.instantiateViewController(withIdentifier: "PlaceDetailViewController") as! PlaceDetailViewController
        detailVC.placeData = placesArray[indexPath.row]
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @IBAction func ClearSearch(sender : UIButton){
        if self.searchTextField.text?.characters.count == 0 {
            
        }else {
            searchTextField.text = ""
            searchTextField.resignFirstResponder()
            self.getPlaces()
        }
    }
}
