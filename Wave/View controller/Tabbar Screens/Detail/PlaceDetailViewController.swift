//
//  PlaceDetailViewController.swift
//  Wave
//
//  Created by Jawad Ahmed on 12/21/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import FCAlertView
class PlaceDetailViewController: UIViewController , UICollectionViewDelegate  {

    @IBOutlet weak var placeImageView: UIImageView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var checkInCountLabel: UILabel!
    @IBOutlet weak var ratioLabel: UILabel!
    @IBOutlet weak var ratingsCountLabel: UILabel!
    
   
    
    fileprivate var imagesArray = [[String: Any]]()
    var placeData: [String: Any]!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detail"
        imagesCollectionView.delegate = self
        imagesCollectionView.dataSource = self
        updateUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    private func updateUI() {
        if let array = placeData["images"] as? [[String: Any]], array.count > 0 {
            imagesArray = array
            let imageDict = imagesArray.first
            placeImageView.sd_setShowActivityIndicatorView(true)
            placeImageView.sd_setImage(with: URL.init(string: imageDict?["image_path"] as! String), completed: nil)
        }
        else {
            placeImageView.image = nil
        }
        
        phoneLabel.text = placeData["phone"] as? String
        placeNameLabel.text = placeData["name"] as? String
        addressLabel.text = placeData["address"] as? String
        detailLabel.text = placeData["description"] as? String
        if let checkInCount = placeData["check_in_count"] as? String{
            if placeData["check_in_count"] != nil  {
                checkInCountLabel.text = String.init(describing: placeData["check_in_count"]!)
            }else {
                checkInCountLabel.text = ""
            }
        }
        else{
           checkInCountLabel.text = ""
        }
        
        ratingsCountLabel.text = "( " + String.init(describing: placeData["no_of_ratings"]!) + " Ratings" + " )"
    }
    @IBAction func checkInBtn(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let params = ["place_id": self.placeData["id"] as! Int,
                      "check_in": dateFormatter.string(from: Date())
                      
            ] as [String: Any]
        
        
        APIWrapper.postRequest(with: "user/checkin", params: params, image: nil) { (response) in
            
        }
    }
    
    @IBAction func rateBtn(_ sender: Any) {
        let alert = FCAlertView()
        
        print(self.placeData)
        alert.colorScheme = UIColor.init(red: (34/255), green: (150/255), blue: (240/255), alpha: 1.0)
        alert.makeAlertTypeRateStars { (rating) in
            print("your rating is \(rating)")
            let ratingString = "\(rating)"
            let params = ["rating": ratingString , "place_id" : self.placeData["id"] as! Int] as [String : Any]
            print(params)

            APIWrapper.postRequest(with: "rate/place", params: params, image: nil) { (response) in
            
            }
            
            
        }
        alert.showAlert(inView: self,
                        withTitle: "Rating",
                        withSubtitle: "Please give rating to place",
                        withCustomImage: nil,
                        withDoneButtonTitle: "Done",
                        andButtons: nil)
    }
}


extension PlaceDetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath)
        let imageDict = imagesArray[indexPath.row]
        (cell.viewWithTag(25) as! UIImageView).sd_setImage(with: URL.init(string: imageDict["image_path"] as! String), completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageDict = imagesArray[indexPath.row]
        
        self.placeImageView.sd_setImage(with: URL.init(string: imageDict["image_path"] as! String), completed: nil)
    }
    
    
}

extension PlaceDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableCell")
        let leftLabel = cell?.viewWithTag(1) as! UILabel
        let rightLabel = cell?.viewWithTag(2) as! UILabel
        cell?.selectionStyle = .none
        switch indexPath.row {
        case 0:
            leftLabel.text = "Hours Of Operation:"
            rightLabel.text = placeData["hours_of_operation"] as? String
            break
        case 1:
            leftLabel.text = "Happy Hours Time:"
            rightLabel.text = placeData["happy_hours"] as? String
            break
        case 2:
            leftLabel.text = "Open Kitchen"
            rightLabel.text = (placeData["open_kitchen"] as? Bool) == true ? "Yes" : "No"
            break
        case 3:
            leftLabel.text = "Bottle service"
            rightLabel.text = (placeData["bottle_service"] as? Bool) == true ? "Yes" : "No"
            break
        case 4:
            leftLabel.text = "Genre of Music"
            rightLabel.text = placeData["music_genre"] as? String
            break
        case 5:
            leftLabel.text = "Dress Code"
            rightLabel.text = placeData["dress_code"] as? String
            break
        case 6:
            leftLabel.text = "Age Requirement"
            rightLabel.text = placeData["age_requirement"] as? String
            break
        case 7:
            leftLabel.text = "Singles"
            rightLabel.text = String(describing: placeData["no_of_singles"]!)
            rightLabel.textColor = UIColor.yellow
            break
        case 8:
            leftLabel.text = "Average age"
            rightLabel.text = String(describing: placeData["average_age"]!)
            rightLabel.textColor = UIColor.yellow
            break
        default:
            break
        }
        
        return cell!
    }
    
}
