//
//  LeftMenuViewController.swift
//  Wave
//
//  Created by Vengile on 11/07/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import SDWebImage

class LeftMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let path = WaveUser.shared.imagepath {
            let url = URL(string:path)
            profilePic.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "avatar"), options: .progressiveDownload, completed: nil)
        }
        
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2.0
        nameLabel.text = WaveUser.shared.name
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.RefreshAsset(notification:)), name: Notification.Name("RefreshMenu"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.LgoutNotification(notification:)), name: Notification.Name("Logout"), object: nil)

        

        
    }

    func RefreshAsset(notification: NSNotification){
        
//        SDImageCache *imageCache = [SDImageCache sharedImageCache];
//        [imageCache clearMemory];
//        [imageCache clearDisk];

        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        
        if let path = WaveUser.shared.imagepath {
            let url = URL(string:path)
            profilePic.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "avatar"), options: .progressiveDownload, completed: nil)
        }
        
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2.0
        nameLabel.text = WaveUser.shared.name
    }
    
    
    // MARK: - IBActions
    
    @IBAction func logoutAction() {
        DispatchQueue.main.async {
            self.GotoLogin()
//             DataManager.sharedInstance.logoutUser()
//            self.menuContainerViewController.dismiss(animated: true, completion: nil)
//            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "BaseNavigationController") as! BaseNavigationController
//
//            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
//
//            appDel.window?.rootViewController = loginVC
        }
    }
    
    
    func GotoLogin(){
        DataManager.sharedInstance.logoutUser()
        self.menuContainerViewController.dismiss(animated: true, completion: nil)
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "BaseNavigationController") as! BaseNavigationController
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = loginVC
    }
    func LgoutNotification(notification: NSNotification){
        self.GotoLogin()
    }
    
    
    // MARK: - TableView DataSource & Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MenuCell\(indexPath.row)"
        return tableView.dequeueReusableCell(withIdentifier: identifier)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tabbarController = menuContainerViewController.centerViewController as! UITabBarController
        
        if indexPath.row == 0 {
            tabbarController.selectedIndex = 0
        }
        else if indexPath.row == 3 {
            tabbarController.selectedIndex = 2
        }
        else {
            let navigation = tabbarController.viewControllers?[tabbarController.selectedIndex] as! BaseNavigationController
            var viewController: UIViewController
            
            if indexPath.row == 1 {
                viewController = (storyboard?.instantiateViewController(withIdentifier: "PremiumViewController"))!
            }
            else {
                viewController = (storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController"))!
            }
            
            navigation.popToRootViewController(animated: false)
            navigation.pushViewController(viewController, animated: false)
        }
        
        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
}
