//
//  PremiumViewController.swift
//  Wave
//
//  Created by Vengile on 7/12/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class PremiumViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    
    override var hidesBottomBarWhenPushed: Bool {
        get {
            return navigationController?.topViewController == self
        }
        set {
            super.hidesBottomBarWhenPushed = true
        }
    }
    
    
    
    
    // MARK: - IBActions
    
    @IBAction func buyAction() {
        
    }
    
    
    @IBAction func skipAction() {
        _ = navigationController?.popViewController(animated: true)
    }
}
