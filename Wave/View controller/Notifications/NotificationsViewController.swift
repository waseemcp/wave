//
//  NotificationsViewController.swift
//  Wave
//
//  Created by Vengile on 7/11/17.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Notifications"
        (navigationController as! BaseNavigationController).addBackButtonOn(self, selector: #selector(backAction))
        self.tabBarController?.tabBar.isHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    // MARK: - IBActions
    func backAction() {
        _ = navigationController?.popViewController(animated: true)
    }
    

    // MARK: - TableView DataSource & Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell
        
        if (indexPath.row == 0) {
            cell?.setText("You checked in at Temple Night Club", clubName: "Temple Night Club", distance: "10m")
        }
        else {
            cell?.setText("A new party is happening near you at X2 Club. Join and have fun !!", clubName: "X2 Club", distance: "23m")
        }
                
        return cell!
    }

}
