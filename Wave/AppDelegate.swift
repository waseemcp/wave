//
//  AppDelegate.swift
//  Wave
//
//  Created by Vengile on 07/06/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import CoreLocation
import IQKeyboardManager
import MFSideMenu
import FBSDKCoreKit
import FBSDKLoginKit
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
    var locationManager: CLLocationManager?


	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
  
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        locationManager = CLLocationManager()
        locationManager?.requestWhenInUseAuthorization()
        
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
        UIApplication.shared.statusBarStyle = .lightContent
        
        if (DataManager.sharedInstance.getPermanentlySavedUser() != nil) {
            DataManager.sharedInstance.user = DataManager.sharedInstance.getPermanentlySavedUser()
            WaveUser.shared.age = Int((DataManager.sharedInstance.user?.age)!)
            WaveUser.shared.email = DataManager.sharedInstance.user?.email
            WaveUser.shared.fbId = Int((DataManager.sharedInstance.user?.FBID)!)
            WaveUser.shared.gender = DataManager.sharedInstance.user?.gender
            WaveUser.shared.imagepath = DataManager.sharedInstance.user?.imagepath
            WaveUser.shared.isAdmin = DataManager.sharedInstance.user?.isAdmin
            WaveUser.shared.martialStatus = Int((DataManager.sharedInstance.user?.martialStatus)!)
            WaveUser.shared.id = Int((DataManager.sharedInstance.user?.ID)!)
            WaveUser.shared.sessionId = DataManager.sharedInstance.user?.sessionID
            WaveUser.shared.name = DataManager.sharedInstance.user?.userName
            self.showTabbar()
        }
		return true
	}

	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}

	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
    func showTabbar() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabbar = storyboard.instantiateViewController(withIdentifier: "WaveTabbarViewController")
        let leftMenu = storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        let mfContainer = storyboard.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
        
        mfContainer.centerViewController = tabbar
        mfContainer.leftMenuViewController = leftMenu
        self.window = UIWindow(frame: UIScreen.main.bounds)
        mfContainer.leftMenuWidth = (self.window?.frame.size.width)! - 100
        mfContainer.panMode = .init(0)
        self.window?.rootViewController = mfContainer
       
    }

    private func application(application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        
        
        return  FBSDKApplicationDelegate.sharedInstance().application(application,open: url as URL!,sourceApplication: sourceApplication,annotation: annotation)
        
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if #available(iOS 9.0 , *){
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: "UIApplicationOpenURLOptionsKey", annotation: nil)
            
        }
        
        return true
    }
    
}

