//
//  CustomAnnot.swift
//  Wave
//
//  Created by Waqar Khalid on 1/1/18.
//  Copyright © 2018 Wave. All rights reserved.
//

import UIKit
import MapKit
class CustomAnnot: NSObject,MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    var title: String?
  
    var location: Location
    
    init(title: String, coordinate: CLLocationCoordinate2D, location: Location) {
        self.title = title
        self.location = location
        self.coordinate = coordinate
    }
}
