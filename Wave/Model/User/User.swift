//
//  User.swift
//  DrakeMaster
//
//  Created by Apple on 29/12/2016.
//  Copyright © 2016 Apple. All rights reserved.
//


import Foundation
import UIKit
import CoreLocation
class User: NSObject, NSCopying, NSCoding {
    
    
    var userName	= kEmptyString
    var email		= kEmptyString
    var password	= kEmptyString
	var ID			= kEmptyString
    var FBID			= kEmptyString
    var deviceID	= kEmptyString
    var deviceType	= "ios"
    var sessionID	= kEmptyString
    var profilePictureURL = kEmptyString
    var age		: Int?
	var gender	= kEmptyString
    
    var User_lat	= kEmptyString
    var User_long	= kEmptyString
    var profileImage = UIImage.init(named: "profile-placeholder")
    var isAdmin :Bool?
    var martialStatus :Int?
    var imagepath = kEmptyString
    var location: CLLocation?
    var currentSearchText = ""
    var currentSliderRadius: Int = 100
    var radiusValue:Float?
    
    convenience init(json: [String: AnyObject]?) {
        self.init()
        
        print(json!)
        self.email		= json?[kEmailKey] as? String ?? kEmptyString
        self.password	= json?[kPasswordKey] as? String ?? kEmptyString
		
		self.sessionID	= json?[kSessionKey] as? String ?? kEmptyString
        self.ID	= String(json?[kUser_ID] as? Int ?? 0)
        
        self.User_lat	= ""
        self.User_long	= ""
        self.radiusValue = 100.00
        
        self.FBID	= String(json?[kUser_fb_id] as? Int ?? 0)
        
        self.userName	= json?[kUser_name] as? String ?? kEmptyString
        self.gender	= json?[gender] as? String ?? kEmptyString
        self.profilePictureURL	= json?[kUser_user_image] as? String ?? kEmptyString
		self.age	= json?[kUser_age] as? Int
        self.isAdmin = json?["is_admin"] as? Bool
        self.martialStatus = json?["marital_status"] as? Int
        self.imagepath = json?["user_image"] as? String ?? kEmptyString
        
	}
    
    func encode(with aCoder: NSCoder) {
		aCoder.encode(email, forKey: kEmailKey)
		aCoder.encode(password, forKey: kPasswordKey)
        aCoder.encode(userName, forKey: kUser_name)
        aCoder.encode(ID, forKey: kUser_ID)
        aCoder.encode(FBID, forKey: kUser_fb_id)
        aCoder.encode(sessionID, forKey: kSessionKey)
        aCoder.encode(age, forKey: kUser_age)
        aCoder.encode(User_long, forKey: "User_long")
        aCoder.encode(User_lat, forKey: "User_lat")
        aCoder.encode(gender, forKey: kUser_gender)
        print(profilePictureURL)
        aCoder.encode(profilePictureURL, forKey: "user_image1")
        aCoder.encode(profileImage, forKey: "user_image")
        aCoder.encode(isAdmin,forKey: "is_admin")
        aCoder.encode(martialStatus,forKey: "marital_status")
        aCoder.encode(imagepath,forKey: "image_path")
        aCoder.encode(radiusValue,forKey: "radius_value")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self.email = aDecoder.decodeObject(forKey:kEmailKey) as? String ?? kEmptyString
        self.password = aDecoder.decodeObject(forKey:kPasswordKey) as? String ?? kEmptyString
        
        self.User_long = aDecoder.decodeObject(forKey:"User_long") as? String ?? kEmptyString
        self.User_lat = aDecoder.decodeObject(forKey:"User_lat") as? String ?? kEmptyString
        self.userName = aDecoder.decodeObject(forKey:kUser_name) as? String ?? kEmptyString
        
        self.ID = aDecoder.decodeObject(forKey:kUser_ID) as? String ?? kEmptyString
        
        self.FBID = aDecoder.decodeObject(forKey:kUser_fb_id) as? String ?? kEmptyString
        self.sessionID = aDecoder.decodeObject(forKey:kSessionKey) as? String ?? kEmptyString
        
        self.age = aDecoder.decodeObject(forKey:kUser_age) as? Int
        self.gender = aDecoder.decodeObject(forKey:kUser_gender) as? String ?? kEmptyString

        self.profilePictureURL = aDecoder.decodeObject(forKey:"user_image1") as? String ?? kEmptyString
        self.isAdmin = aDecoder.decodeObject(forKey:"is_admin") as? Bool
        self.martialStatus = aDecoder.decodeObject(forKey:"marital_status") as? Int
        self.imagepath = aDecoder.decodeObject(forKey:"image_path") as? String ?? kEmptyString
        self.radiusValue = aDecoder.decodeObject(forKey:"radius_value") as? Float
}
	
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = User()

        return copy
    }

    func toJSON() -> [String: AnyObject] {
        var json = [String: AnyObject]()
        json[kEmailKey] = self.email as AnyObject?
        json[kPasswordKey] = self.password as AnyObject?

        json[kUser_name] = self.userName as AnyObject?
        json[kUser_fb_id] = self.FBID as AnyObject?

        json[kSessionKey] = self.sessionID as AnyObject?
        json[kUser_age] = self.age as AnyObject?

        json[kUser_gender] = self.gender as AnyObject?
        
        if self.ID.characters.count > 0 {
            if Int(self.ID)! > 0 {
                json[kUser_ID] = self.ID as AnyObject?
            }
        }
        
        if self.FBID.characters.count > 0 {
            if Int(self.FBID)! > 0 {
                json[kUser_fb_id] = self.FBID as AnyObject?
            }
        }
        
        
        print(json)
        return json
    }
}
