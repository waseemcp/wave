//
//  DataManager.swift
//  DrakeMaster
//
//  Created by Apple on 29/12/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    
    var user: User? {
        didSet {
            saveUserPermanentally()
        }
    }
	
    var lastKnownAddress = kEmptyString
//    var registerType = RegisterType.Email.rawValue
    static let sharedInstance = DataManager()
	var APIHitTry = 1
    
    func logoutUser() {
        user = nil
        lastKnownAddress = kEmptyString
		UserDefaults.standard.removeObject(forKey: "user")
    }
    
    func saveUserPermanentally() {
        if user != nil {
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: user!)
            UserDefaults.standard.set(encodedData, forKey: "user")
        }else {
            UserDefaults.standard.removeObject(forKey: "user")
        }
    }
    
    func getPermanentlySavedUser() -> User? {
        if let data = UserDefaults.standard.data(forKey: "user"),
            let userData = NSKeyedUnarchiver.unarchiveObject(with: data) as? User {
            return userData
        } else {
            return nil
        }
    }
}
