 //
//  NetworkManager.swift
//  DrakeMaster
//
//  Created by Apple on 29/12/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import WebKit
import GCNetworkReachability
import CoreLocation

class NetworkManager: NSObject {
	
	
    class func isNetworkReachable() -> Bool {
        let reachablity = GCNetworkReachability(internetAddressString: kServerIPAddress)
        let reachable = reachablity?.isReachable()
        reachablity?.stopMonitoringNetworkReachability()
        return reachable!
    }
	
    private class func sendJSONToServer(jsonParam: [String: AnyObject]?, webserviceName: String, isPostRequest: Bool = true, completion: @escaping (_ success: Bool, _ message: String, _ response: [String: AnyObject]?) -> Void) -> Request? {
        let json = jsonParam
        var headers = [String:String] ()
        
        if NetworkManager.isNetworkReachable() {
            let postURLString = kBaseURLString + webserviceName
            
            if let userSession = DataManager.sharedInstance.user?.sessionID {
                headers[kUserSessionKey] = userSession
            }
            
            print(headers)
            print(json!)
            
            Alamofire.request(postURLString, method: isPostRequest ? .post : .get, parameters: json ,headers : headers).responseJSON(completionHandler: { (response) in
                print(response)
                if response.result.value != nil { //Server Reachable
                    let resultValue = response.result.value!
                    let success = (resultValue as? [String: AnyObject])?["status"] as? Int == 1
                    var errorMsg = ""
                    
                    print(success)
                    
                    if !success {
                        let mainResponse    = resultValue as! [String: AnyObject]
                        let errorMain       = mainResponse["error"] as! [String: AnyObject]
                        let errorMsgArray    = errorMain["messages"] as! [String]
                        errorMsg = errorMsgArray[0]
                        
                    }
                    
                    completion(success, errorMsg, response.result.value as! [String : AnyObject]?)
                }else {//Server Not Reachable
                    completion(false, kServerNotReachableMessage, nil)
                }
            })
            
        }else {
            completion(false, kNetworkNotAvailableMessage, nil)
        }
        return nil
    }
    
 
    
	class func nsdataToJSON(data: Data) -> AnyObject? {
		do {
			
			
			return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as AnyObject
		} catch let myJSONError {
			print(myJSONError)
		}
		return nil
	}
	
	

    //MARK:
    //MARK: Login APIS

	
	class func login(user: User, completion: @escaping (_ success: Bool, _ message: String, _ updatedUser: User , _ response: [String: AnyObject]?) -> Void ) {
        
        
        _ = NetworkManager.sendJSONToServer(jsonParam: user.toJSON(), webserviceName: WebServiceName.Login.rawValue) { (success, message, response) -> Void in
			var user = User()
			
			if success {
				
				print(response!)
                let dataDict  = response?["data"] as! [String : Any]
                
				user = User(json: dataDict as [String : AnyObject]?)
			}
			completion(success, message, user , response)
		}
	}
    
    class func SignUpusers(user: User, completion: @escaping (_ success: Bool, _ message: String, _ updatedUser: User , _ response: [String: AnyObject]?) -> Void ) {
        _ = NetworkManager.sendJSONToServer(jsonParam: user.toJSON(), webserviceName: WebServiceName.Register.rawValue) { (success, message, response) -> Void in
            let user = User()
            
            completion(success, message, user , response)
        }
    }
	

    
    class func ForGotEmail(user: User, completion: @escaping (_ success: Bool, _ message: String, _ updatedUser: User , _ response: [String: AnyObject]?) -> Void ) {
        _ = NetworkManager.sendJSONToServer(jsonParam: user.toJSON(), webserviceName: WebServiceName.ForgotPassword.rawValue) { (success, message, response) -> Void in
            
            print(response!)
            print(message)
            
            completion(success, message, user , response)
        }
    }
    
    class func register(user: User, completion: @escaping (_ success: Bool, _ message: String, _ updatedUser: User,_ response:[String: AnyObject]) -> Void ) {
        
        let headers = ["": ""]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                let imageData = UIImagePNGRepresentation(user.profileImage!)!
                multipartFormData.append(imageData, withName: "user_image", fileName: "file.png", mimeType: "image/png")
                
                multipartFormData.append("\(user.userName)".data(using: .utf8)!, withName: kUser_name)
                multipartFormData.append("\(user.email)".data(using: .utf8)!, withName: kEmailKey)
                multipartFormData.append("\(user.password)".data(using: .utf8)!, withName: kPasswordKey)
                multipartFormData.append("\(user.deviceID)".data(using: .utf8)!, withName: kUser_device_id)
                multipartFormData.append("\(user.deviceType)".data(using: .utf8)!, withName: kUser_device_type)
                multipartFormData.append("\(user.FBID)".data(using: .utf8)!, withName: kUser_fb_id)
                multipartFormData.append("\(user.gender)".data(using: .utf8)!, withName: kUser_gender)
                multipartFormData.append("\(user.age)".data(using: .utf8)!, withName: kUser_age)
//                print("multipart form data is \(multipartFormData)")
//                print(kBaseURLString + WebServiceName.Register.rawValue)
//                print("headers are \(headers)")
        },
            to:  "http://139.162.37.73/wave_app/public/api/user/fblogin",
            method: .post,
            headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    print(upload)
                    print(upload.responseJSON)
                    upload.responseJSON { response in
                        if response.result.value != nil { //Server Reachable
                            let resultValue = response.result.value! as! [String : Any]
                            let success = resultValue["status"] as? Int == 1
                            if success{
                                let sessions = resultValue["data"] as? [String: AnyObject]
                                let errorMsg = resultValue["errorMessage"] as? String ?? kEmptyString
                                let user = User(json: sessions!)
                                completion(success, errorMsg, user,sessions!)
                            } else {
                                
                                let mainResponse    = resultValue as [String: AnyObject]
                                let errorMain       = mainResponse["error"] as! [String: AnyObject]
                                let errorMsgArray    = errorMain["messages"] as! [String]
                                let errorMsg = errorMsgArray[0]
                                
                                
                                
                                completion(false, errorMsg, User(),mainResponse)
                            }
                        } else {//Server Not Reachable
                            var emptyData = [String:AnyObject]()
                            completion(false, kServerNotReachableMessage, User(),emptyData)
                        }
                    }
                case .failure( _):
                    var emptyData = [String:AnyObject]()
                    completion(false, kServerNotReachableMessage, User(),emptyData)
                }
        }
        )
    }

    class func GetPlaces(UserLocation: [String : AnyObject] , completion: @escaping (_ success: Bool, _ message: String , _ response: [String: AnyObject]?) -> Void ) {
        _ = NetworkManager.sendJSONToServer(jsonParam: UserLocation, webserviceName: WebServiceName.GetPlaces.rawValue , isPostRequest: false) { (success, message, response) -> Void in
            
            completion(success, message , response)
        }
    }
    
    
    class func searchPlaces(UserLocation: [String : AnyObject] , completion: @escaping (_ success: Bool, _ message: String , _ response: [String: AnyObject]?) -> Void ) {
        _ = NetworkManager.sendJSONToServer(jsonParam: UserLocation, webserviceName: WebServiceName.searchPlace.rawValue , isPostRequest: false) { (success, message, response) -> Void in
            completion(success, message , response)
        }
    }
    
    class func changePassword(passwordParams: [String : AnyObject] , completion: @escaping (_ success: Bool, _ message: String , _ response: [String: AnyObject]?) -> Void ) {
        _ = NetworkManager.sendJSONToServer(jsonParam: passwordParams, webserviceName: WebServiceName.ChangePassword.rawValue , isPostRequest: true) { (success, message, response) -> Void in
            completion(success, message , response)
        }
    }
    
    
    class func CheckIn(placeID: [String : AnyObject], completion: @escaping (_ success: Bool, _ message: String , _ response: [String: AnyObject]?) -> Void ) {
        
        print(placeID)
        _ = NetworkManager.sendJSONToServer(jsonParam: placeID, webserviceName: WebServiceName.CheckIn.rawValue) { (success, message, response) -> Void in
            
            print(response!)
            print(message)
            
            completion(success, message , response)
        }
    }
    class func RatePlace(placeID: [String : AnyObject], completion: @escaping (_ success: Bool, _ message: String , _ response: [String: AnyObject]?) -> Void ) {
        
        print(placeID)
        _ = NetworkManager.sendJSONToServer(jsonParam: placeID, webserviceName: WebServiceName.RatePlace.rawValue) { (success, message, response) -> Void in
            
            print(response!)
            print(message)
            
            completion(success, message , response)
        }
    }
    
}
