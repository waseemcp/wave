//
//  User.swift
//  DrakeMaster
//
//  Created by Apple on 29/12/2016.
//  Copyright © 2016 Apple. All rights reserved.
//


import Foundation
import UIKit

class Location: NSObject{
    
    
    var address             = kEmptyString
    var age_requirement		= kEmptyString
    var bottle_service      = kEmptyString
	var check_in_count		= kEmptyString
    var locationDescription			= kEmptyString
    var dress_code          = kEmptyString
    var distance          = kEmptyString
    var female_count        = "ios"
    var happy_hours         = kEmptyString
    var hours_of_operation  = kEmptyString
	var id                  = kEmptyString
	var lat                 = kEmptyString
    
    var lon                 = kEmptyString
    var male_count          = kEmptyString
    var music_genre         = kEmptyString
    var name                = kEmptyString
    var open_kitchen        = kEmptyString
    var phone               = kEmptyString
    var price               = kEmptyString
    var rating              = kEmptyString
    var imagesArray         = [[String : Any]]()
    

    convenience init(json: [String: AnyObject]?) {
        self.init()
        
        print(json!)
        self.address		= json?["address"] as? String ?? kEmptyString
        self.dress_code	= json?["dress_code"] as? String ?? kEmptyString
		self.distance	= String((json?["distance"] as? Double ?? 0.0).rounded(toPlaces: 2))
        self.locationDescription	= json?["description"] as? String ?? kEmptyString
        
        self.dress_code	= json?["dress_code"] as? String ?? kEmptyString
        self.happy_hours	= json?["happy_hours"] as? String ?? kEmptyString
        self.hours_of_operation	= json?["hours_of_operation"] as? String ?? kEmptyString
        self.lat	= String(json?["lat"] as? Double ?? 0.0)
        self.lon	= String(json?["lon"] as? Double ?? 0.0)
        self.music_genre	= json?["music_genre"] as? String ?? kEmptyString
        self.name	= json?["name"] as? String ?? kEmptyString
        self.phone	= json?["phone"] as? String ?? kEmptyString
        self.price	= json?["price"] as? String ?? kEmptyString
        
        self.imagesArray = (json?["images"] as? [[String : Any]])!
        
        self.id	= String(json?["id"] as? Int ?? 0)
        self.age_requirement	= String(json?["age_requirement"] as? Int ?? 0)
        self.bottle_service	= String(json?["bottle_service"] as? Int ?? 0)
        self.check_in_count	= String(json?["check_in_count"] as? Int ?? 0)
        self.female_count	= String(json?["female_count"] as? Int ?? 0)
        self.male_count	= String(json?["male_count"] as? Int ?? 0)
        self.open_kitchen	= String(json?["open_kitchen"] as? Int ?? 0)
        self.id	= String(json?["id"] as? Int ?? 0)
        self.rating	= String(json?["rating"] as? Int ?? 0)
        
        
        
	}
    
   
    func toJSON() -> [String: AnyObject] {
        var json = [String: AnyObject]()
//        json[kEmailKey] = self.email as AnyObject?
//        json[kPasswordKey] = self.password as AnyObject?
//
//        json[kUser_name] = self.userName as AnyObject?
//        json[kUser_fb_id] = self.FBID as AnyObject?
//
//        json[kSessionKey] = self.sessionID as AnyObject?
//        json[kUser_age] = self.age as AnyObject?
//
//        json[kUser_gender] = self.gender as AnyObject?
//        
//        if self.ID.characters.count > 0 {
//            if Int(self.ID)! > 0 {
//                json[kUser_ID] = self.ID as AnyObject?
//            }
//        }
//        
        
        print(json)
        return json
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
