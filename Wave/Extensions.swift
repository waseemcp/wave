//
//  Extensions.swift
//  SaveMe
//
//  Created by Vengile on 26/04/2017.
//  Copyright © 2017 Vengile. All rights reserved.
//

import Foundation
import MBProgressHUD
import UIKit

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        
        return controller
    }
}


extension UIImage {
    
//    func imageResize (sizeChange:CGSize)-> UIImage{
//        
//        let hasAlpha = true
//        let scale: CGFloat = 0.0 // Use scale factor of main screen
//        
//        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
//        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
//        
//        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
//        return scaledImage!
//    }
    
}


//MARK:- UIView
extension UIView
{
    func roundCorners(corners:UIRectCorner, radius: CGFloat)
    {

        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft , .bottomRight , .topLeft], cornerRadii: CGSize(width:radius, height:radius)).cgPath
        
        self.layer.backgroundColor = UIColor.green.cgColor
        self.layer.mask = rectShape
    }
    
    
    func CornerRadiousView()  {
        self.layer.cornerRadius = Constants.kCornerRaious
        
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = true
    }
    
    
    func showLoading() {
        DispatchQueue.main.async {
           let hud = MBProgressHUD.showAdded(to: self, animated: true)
            hud.label.text = "Loading.."
        }
    }
    func hideLoading() {
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self, animated: true)
        }
    }
    
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    }
    
}

extension Date {
    func GetString(dateFormate : String) -> String{
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = dateFormate
        
        return dateFormatterGet.string(from: self)
        
        
    }
	
	

		func toString() -> String {
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "hh:mm a"
			return dateFormatter.string(from: self)
		}

}


extension String {
    func GetDateFormate() -> String {
        
        print(self)
        if self.characters.count > 0 {
           
            
            if Int(self)! > 0 {
                let endIndex = self.index(self.endIndex, offsetBy: -3)
                let truncated = self.substring(to: endIndex)
                let date = Date(timeIntervalSince1970: Double(truncated)!)
                return date.GetString(dateFormate: "YYYY-MM-dd")
            }else {
               return "" 
            }
            
           
        }else {
            return ""
        }
    }
}


