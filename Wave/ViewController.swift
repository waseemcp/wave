//
//  ViewController.swift
//  Wave
//
//  Created by Vengile on 07/06/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import MFSideMenu
import FacebookCore
import FacebookLogin
class ViewController: BaseViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
		
	}


	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.isNavigationBarHidden = true
     
	}
	
	//MARK: Button Actions
	//MARK:
	
	@IBAction func GotoTermCondition(sender : UIButton){
		self.ShowViewWithIdentifier(name: "TermsNavigation")
	}
	
	@IBAction func SignIN(sender : UIButton){
		self.PushViewLogin(ShowLoginView: true)
	}
	
	@IBAction func SignUp(sender : UIButton){
		self.PushViewLogin(ShowLoginView: false)
	}
	
	@IBAction func FaceBookLogin(sender : UIButton){
       self.loginWithFacebook()
	}
    func loginWithFacebook() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions:[ .publicProfile,.email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
                
            case .cancelled:
                print("User cancelled login.")
            case .success(grantedPermissions: let grantedPermissions, declinedPermissions: let declinedPermissions, token: let accessToken):
                let params: [String : Any]? = ["fields": "id, name, email"]
                
                let graphRequest = GraphRequest(graphPath: "/me", parameters: params!)
                graphRequest.start {
                    (urlResponse, requestResult) in
                    
                    switch requestResult {
                    case .failed(let error):
                        print("error in graph request:", error)
                        break
                    case .success(let graphResponse):
                        if let responseDictionary = graphResponse.dictionaryValue {
                            print(responseDictionary)
                            self.showLoading()
                            
                            let user = User()
                            
                            user.userName = responseDictionary["name"] as! String
                            user.FBID = responseDictionary["id"] as! String
                            user.email = responseDictionary["email"] as! String
                            let pictureUrlString = "https://graph.facebook.com/\(user.FBID)/picture?type=large&return_ssl_resources=1"
                            let pictureUrl = URL(string: pictureUrlString)
                            let data = try? Data(contentsOf: pictureUrl!)
                            
                            if data != nil {
                                let image = UIImage(data: data!)
                                user.profileImage = image
                            }
                            
                            user.deviceID = "Dummy token"
                            user.deviceType = "ios"
                            user.age = 0
                            
                            user.gender = "male"
                            NetworkManager.register(user: user, completion: { (success, message, updatedUser,response) in
                                self.hideLoading()
                                if success {
                                    WaveUser.shared.create(with: response as [String: Any])
                                    DataManager.sharedInstance.user = updatedUser
                                    DataManager.sharedInstance.saveUserPermanentally()
                                    self.GotoHelpPages()
                                }else {
                                    self.ShowErrorAlert(message: message)
                                }
                            })
                            
                            
                        }
                    }
                }
                print ("Logged In")
            }
        }
        
    }
    func GotoHelpPages(){
        self.PushViewWithIdentifier(name: "HelpPagesViewController")
    }
//    func showTabbar() {
//        let tabbar = storyboard?.instantiateViewController(withIdentifier: "WaveTabbarViewController")
//        let leftMenu = storyboard?.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
//        let mfContainer = storyboard?.instantiateViewController(withIdentifier: "MFSideMenuContainerViewController") as! MFSideMenuContainerViewController
//
//        mfContainer.centerViewController = tabbar
//        mfContainer.leftMenuViewController = leftMenu
//        mfContainer.leftMenuWidth = view.frame.size.width - 100
//        mfContainer.panMode = .init(0)
//
//        present(mfContainer, animated: true, completion: nil)
//    }
}

