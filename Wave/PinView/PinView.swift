//
//  PinView.swift
//  Wave
//
//  Created by Jawad Ahmed on 17/07/2017.
//  Copyright © 2017 Wave. All rights reserved.
//

import UIKit
import MFSideMenu
class PinView: UIView {

    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var eventImgView: UIImageView!
    @IBOutlet weak var chargesLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var eventTitleLbl: UILabel!
    @IBOutlet weak var peopleCountLbl: UILabel!

    
    class func loadFromXib() -> PinView {
        let pinView = Bundle.main.loadNibNamed("PinView", owner: self, options: nil)?[0] as! PinView
       
        return pinView
    }
    
    
    
    func addInView(parentView: UIView,annotation:CustomAnnot) {
        
        frame = CGRect.init(x: 20, y: parentView.frame.size.height-49, width: parentView.frame.size.width-40, height: frame.size.height)
        parentView.addSubview(self)
        
        UIView.animate(withDuration: 0.3) {
            self.frame.origin.y -= self.frame.size.height
        }

        self.setData(annotation: annotation)
    }
    func setData(annotation:CustomAnnot)  {
        if annotation.location.imagesArray.count>0{
            let imagesDic = annotation.location.imagesArray[0]
            let urlString = imagesDic["image_path"]
            let url = URL(string:urlString as! String)
             eventImgView.sd_setImage(with: url, completed: nil)
        }
      let rating = Int(annotation.location.rating)!
        switch rating {
        case 0:
            star1.image = #imageLiteral(resourceName: "g_star")
            star2.image = #imageLiteral(resourceName: "g_star")
            star3.image = #imageLiteral(resourceName: "g_star")
            star4.image = #imageLiteral(resourceName: "g_star")
            star5.image = #imageLiteral(resourceName: "g_star")
        case 1:
            star1.image = #imageLiteral(resourceName: "b_star")
            star2.image = #imageLiteral(resourceName: "g_star")
            star3.image = #imageLiteral(resourceName: "g_star")
            star4.image = #imageLiteral(resourceName: "g_star")
            star5.image = #imageLiteral(resourceName: "g_star")
        case 2:
            star1.image = #imageLiteral(resourceName: "b_star")
            star2.image = #imageLiteral(resourceName: "b_star")
            star3.image = #imageLiteral(resourceName: "g_star")
            star4.image = #imageLiteral(resourceName: "g_star")
            star5.image = #imageLiteral(resourceName: "g_star")
        case 3:
            star1.image = #imageLiteral(resourceName: "b_star")
            star2.image = #imageLiteral(resourceName: "b_star")
            star3.image = #imageLiteral(resourceName: "b_star")
            star4.image = #imageLiteral(resourceName: "g_star")
            star5.image = #imageLiteral(resourceName: "g_star")
        case 4:
            star1.image = #imageLiteral(resourceName: "b_star")
            star2.image = #imageLiteral(resourceName: "b_star")
            star3.image = #imageLiteral(resourceName: "b_star")
            star4.image = #imageLiteral(resourceName: "b_star")
            star5.image = #imageLiteral(resourceName: "g_star")
        case 5:
            star1.image = #imageLiteral(resourceName: "b_star")
            star2.image = #imageLiteral(resourceName: "b_star")
            star3.image = #imageLiteral(resourceName: "b_star")
            star4.image = #imageLiteral(resourceName: "b_star")
            star5.image = #imageLiteral(resourceName: "b_star")
        default:
            break
        }
        eventTitleLbl.text = annotation.title
        chargesLbl.text = "$\(annotation.location.price) Cover Charge"
        let distanced = Float(annotation.location.distance)
        if let distance = distanced as? Float {
            self.distanceLbl.text = String.init(format: "%.1f Miles Away", distance)
        }
        else {
            self.distanceLbl.text = "Not available"
        }
        distanceLbl.text =  "\(annotation.location.distance) miles away"
        peopleCountLbl.text = annotation.location.check_in_count
       
    }
    
    @IBAction func closeAction() {
        UIView.animate(withDuration: 0.3, animations: { 
            self.frame.origin.y += self.frame.size.height
        })
        { (finished) in
            self.removeFromSuperview()
        }
    }
    @IBAction func showDetailBtn() {
        let nc = NotificationCenter.default
        nc.post(name:Notification.Name(rawValue:"showDetail"),
                object: nil,
                userInfo: nil)
    }
    @IBAction func checkInBtn(_ sender: Any) {
    }
}
